package fr.omega2028.rpgcore.listeners;

import fr.omega2028.rpgcore.model.NameSpaceKeyManager;
import fr.omega2028.rpgcore.model.RPGCatalogManager;
import fr.omega2028.rpgcore.model.items.RPGItem;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.persistence.PersistentDataType;

public class OnPlayerClickEvent implements Listener {
    private final RPGCatalogManager rpgCatalogManager;

    public OnPlayerClickEvent(RPGCatalogManager rpgCatalogManager) {
        this.rpgCatalogManager = rpgCatalogManager;
    }

    @EventHandler
    public void onPlayerClickEvent(PlayerInteractEvent clickEvent){
        if (clickEvent.getPlayer().getInventory().getItemInMainHand().getType() == Material.AIR) return;
        else if (!clickEvent.getPlayer().getInventory().getItemInMainHand().getItemMeta().getPersistentDataContainer()
                .has(NameSpaceKeyManager.getRPGItemKey())) return;
        String id = clickEvent.getPlayer().getInventory().getItemInMainHand().getItemMeta().getPersistentDataContainer()
                .get(NameSpaceKeyManager.getRPGItemKey(), PersistentDataType.STRING);
        if (!rpgCatalogManager.getItemCatalog().containsKey(id)) return;
        RPGItem rpgItem = rpgCatalogManager.getItemCatalog().get(id);
        if (rpgItem == null) return;
        rpgItem.executeComportements(clickEvent);
    }
}
