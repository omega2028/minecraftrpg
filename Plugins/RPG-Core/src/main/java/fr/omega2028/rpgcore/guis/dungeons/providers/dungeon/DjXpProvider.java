package fr.omega2028.rpgcore.guis.dungeons.providers.dungeon;

import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.content.InventoryContents;
import fr.minuskube.inv.content.InventoryProvider;
import fr.omega2028.rpgcore.guis.navigation.GUIManager;
import fr.omega2028.rpgcore.utils.ItemUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.List;

public class DjXpProvider implements InventoryProvider {
    private final GUIManager guiManager;

    public DjXpProvider(GUIManager guiManager) {
        this.guiManager = guiManager;
    }

    @Override
    public void init(Player player, InventoryContents contents) {
        buildDecoration(contents);
        createSaveBtns(player, contents);
        createXPDisplay(player, contents);
        createXpModifiers(player, contents);
    }

    private void createXpModifiers(Player player, InventoryContents contents) {
        contents.set(1, 1, ClickableItem.of(ItemUtils.makeItem(Material.BLACK_DYE,
                ChatColor.RED + "" + ChatColor.BOLD + "-10000"
                        + ChatColor.BOLD + "" + ChatColor.GOLD + "XP"), action -> {
                    guiManager.getNavManager().getNavData(player).getActualDj().setRpgXpGivenAtTheEnd(
                            guiManager.getNavManager().getNavData(player).getActualDj()
                                    .getRpgXpGivenAtTheEnd() - 10000
                    );
                    guiManager.getNavManager().refreshGUI(player);
        }));
        contents.set(1, 2, ClickableItem.of(ItemUtils.makeItem(Material.INK_SAC,
                ChatColor.RED + "" + ChatColor.BOLD + "-1000"
                        + ChatColor.BOLD + "" + ChatColor.GOLD + "XP"), action -> {
                    guiManager.getNavManager().getNavData(player).getActualDj().setRpgXpGivenAtTheEnd(
                            guiManager.getNavManager().getNavData(player).getActualDj()
                                    .getRpgXpGivenAtTheEnd() - 1000
                    );
                    guiManager.getNavManager().refreshGUI(player);
        }));
        contents.set(1, 3, ClickableItem.of(ItemUtils.makeItem(Material.GRAY_DYE,
                ChatColor.RED + "" + ChatColor.BOLD + "-100"
                        + ChatColor.BOLD + "" + ChatColor.GOLD + "XP"), action -> {
                    guiManager.getNavManager().getNavData(player).getActualDj().setRpgXpGivenAtTheEnd(
                            guiManager.getNavManager().getNavData(player).getActualDj()
                                    .getRpgXpGivenAtTheEnd() - 100
                    );
                    guiManager.getNavManager().refreshGUI(player);
        }));
        contents.set(1, 5, ClickableItem.of(ItemUtils.makeItem(Material.LIGHT_BLUE_DYE,
                ChatColor.RED + "" + ChatColor.BOLD + "+100"
                        + ChatColor.BOLD + "" + ChatColor.GOLD + "XP"), action -> {
                    guiManager.getNavManager().getNavData(player).getActualDj().setRpgXpGivenAtTheEnd(
                            guiManager.getNavManager().getNavData(player).getActualDj()
                                    .getRpgXpGivenAtTheEnd() + 100
                    );
                    guiManager.getNavManager().refreshGUI(player);
        }));
        contents.set(1, 6, ClickableItem.of(ItemUtils.makeItem(Material.GLOW_INK_SAC,
                ChatColor.RED + "" + ChatColor.BOLD + "+1000"
                        + ChatColor.BOLD + "" + ChatColor.GOLD + "XP"), action -> {
                    guiManager.getNavManager().getNavData(player).getActualDj().setRpgXpGivenAtTheEnd(
                            guiManager.getNavManager().getNavData(player).getActualDj()
                                    .getRpgXpGivenAtTheEnd() + 1000
                    );
                    guiManager.getNavManager().refreshGUI(player);
        }));
        contents.set(1, 7, ClickableItem.of(ItemUtils.makeItem(Material.BLUE_DYE,
                ChatColor.RED + "" + ChatColor.BOLD + "+10000"
                        + ChatColor.BOLD + "" + ChatColor.GOLD + "XP"), action -> {
                    guiManager.getNavManager().getNavData(player).getActualDj().setRpgXpGivenAtTheEnd(
                            guiManager.getNavManager().getNavData(player).getActualDj()
                                    .getRpgXpGivenAtTheEnd() + 10000
                    );
                    guiManager.getNavManager().refreshGUI(player);
        }));
    }

    private void createXPDisplay(Player player, InventoryContents contents) {
        contents.set(2, 4, ClickableItem.empty(ItemUtils.makeItemAndDescription(Material.COMPARATOR,
                ChatColor.WHITE + "XP reçue à la fin du donjon",
                List.of(ChatColor.GOLD + "" + ChatColor.BOLD +
                        guiManager.getNavManager().getNavData(player).getActualDj().getRpgXpGivenAtTheEnd()
                + "XP" ))));
    }

    private void createSaveBtns(Player player, InventoryContents contents) {
        contents.set(3, 8, ClickableItem.of(ItemUtils.makeItem(Material.GREEN_CONCRETE,
                ChatColor.GREEN + "SAVE"), action -> {
            guiManager.getNavManager().openInventory(player, guiManager.getDjEditGUI());
        }));
        contents.set(3, 0, ClickableItem.of(ItemUtils.makeItem(Material.GREEN_CONCRETE,
                ChatColor.GREEN + "SAVE"), action -> {
            guiManager.getNavManager().openInventory(player, guiManager.getDjEditGUI());
        }));
    }

    @Override
    public void update(Player player, InventoryContents contents) {

    }

    private void buildDecoration(InventoryContents inventoryContents){
        ItemStack deco = ItemUtils.makeItem(Material.BLACK_STAINED_GLASS_PANE, " ");
        for (int slot : Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 17, 18, 19, 25, 26, 28, 29, 30, 31, 32, 33, 34)){
            inventoryContents.set(slot / 9, slot % 9, ClickableItem.empty(deco));
        }
    }
}
