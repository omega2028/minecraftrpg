package fr.omega2028.rpgcore.model.spawner;

import fr.omega2028.rpgcore.model.monsters.RPGMonster;
import org.bukkit.Location;

/**
 * An RPG Spawner but automatic
 */
public class RPGAutomaticSpawner extends RPGSpawner{
    private int activationReach;
    private int secondsBetweenSpawn;

    public RPGAutomaticSpawner(String spawnerID) {
        super(spawnerID);
    }

    public boolean isSpawnerActivated(){
        return spawnLoc.getNearbyPlayers(activationReach).size() != 0;
    }

    public int getActivationReach() {
        return activationReach;
    }

    public void setActivationReach(int activationReach) {
        this.activationReach = activationReach;
    }

    public int getSecondsBetweenSpawn() {
        return secondsBetweenSpawn;
    }

    public void setSecondsBetweenSpawn(int secondsBetweenSpawn) {
        this.secondsBetweenSpawn = secondsBetweenSpawn;
    }
}
