package fr.omega2028.rpgcore.guis.dungeons.providers.rooms;

import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.content.InventoryContents;
import fr.minuskube.inv.content.InventoryProvider;
import fr.omega2028.rpgcore.guis.navigation.GUIManager;
import fr.omega2028.rpgcore.model.dungeon.RPGDjRoom;
import fr.omega2028.rpgcore.utils.Decorator;
import fr.omega2028.rpgcore.utils.GeometryUtils;
import fr.omega2028.rpgcore.utils.ItemUtils;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.*;

public class DjEditRoomProvider implements InventoryProvider {
    private final GUIManager guiManager;
    private final JavaPlugin plugin;
    private final Map<UUID, Integer> sphereTasks = new HashMap<>();

    public DjEditRoomProvider(GUIManager guiManager, JavaPlugin plugin) {
        this.guiManager = guiManager;
        this.plugin = plugin;
    }

    @Override
    public void init(Player player, InventoryContents contents) {
        buildDecoration(contents);
        createBackButton(player, contents);
        createSetCenterButton(player, contents);
        createShowSizeButton(player, contents);
        createSetEntryButton(player, contents);
        createSetExitButton(player, contents);
    }

    private void createSetExitButton(Player player, InventoryContents contents) {
        Location exit = guiManager.getNavManager().getNavData(player).getActualRoom().getExit();
        ItemStack icon;
        if (guiManager.getNavManager().getNavData(player).getActualRoom().getCenter() == null)
            icon = ItemUtils.makeItemAndDescription(Material.RED_STAINED_GLASS_PANE, ChatColor.RED + "Sortie",
                    List.of(ChatColor.RED + "" + ChatColor.BOLD + "Le CENTRE n'est pas définit !"));
        else if (exit == null)
            icon = ItemUtils.makeItemAndDescription(Material.RED_STAINED_GLASS_PANE, ChatColor.RED + "Sortie",
                    List.of(ChatColor.GRAY + "La sortie n'est pas définie"));
        else
            icon = ItemUtils.makeItemAndDescription(Material.GREEN_STAINED_GLASS_PANE, ChatColor.GREEN + "Sortie",
                    List.of(ChatColor.GRAY + "La sortie est définie en : ",
                            ChatColor.GRAY + "X : " + ChatColor.WHITE + exit.getX(),
                            ChatColor.GRAY + "Y : " + ChatColor.WHITE + exit.getY(),
                            ChatColor.GRAY + "Z : " + ChatColor.WHITE + exit.getZ(),
                            ChatColor.GRAY + "Monde : " + ChatColor.WHITE + exit.getWorld()));
        contents.set(2, 7, ClickableItem.of(icon, action -> {
            if (guiManager.getNavManager().getNavData(player).getActualRoom().getCenter() == null) return;
            if (!guiManager.getNavManager().getNavData(player).getActualRoom().isInRoom(player.getLocation())){
                player.sendMessage(Decorator.convertError("La sortie doit se situer dans le rayon d'action de la pièce !"));
                guiManager.getNavManager().closeInventory(player);
                guiManager.getNavManager().messageReopenGUI(player);
            }
            else {
                guiManager.getNavManager().getNavData(player).getActualRoom().setExit(player.getLocation());
            }
            guiManager.getNavManager().refreshGUI(player);
        }));
    }

    private void createSetEntryButton(Player player, InventoryContents contents) {
        Location entry = guiManager.getNavManager().getNavData(player).getActualRoom().getEntry();
        ItemStack icon;
        if (guiManager.getNavManager().getNavData(player).getActualRoom().getCenter() == null)
            icon = ItemUtils.makeItemAndDescription(Material.RED_STAINED_GLASS_PANE, ChatColor.RED + "Entrée",
                    List.of(ChatColor.RED + "" + ChatColor.BOLD + "Le CENTRE n'est pas définit !"));
        else if (entry == null)
            icon = ItemUtils.makeItemAndDescription(Material.RED_STAINED_GLASS_PANE, ChatColor.RED + "Entrée",
                    List.of(ChatColor.GRAY + "L'entrée n'est pas définie"));
        else
            icon = ItemUtils.makeItemAndDescription(Material.GREEN_STAINED_GLASS_PANE, ChatColor.GREEN + "Entrée",
                    List.of(ChatColor.GRAY + "L'entrée est définie en : ",
                            ChatColor.GRAY + "X : " + ChatColor.WHITE + entry.getX(),
                            ChatColor.GRAY + "Y : " + ChatColor.WHITE + entry.getY(),
                            ChatColor.GRAY + "Z : " + ChatColor.WHITE + entry.getZ(),
                            ChatColor.GRAY + "Monde : " + ChatColor.WHITE + entry.getWorld()));
        contents.set(2, 1, ClickableItem.of(icon, action -> {
            if (guiManager.getNavManager().getNavData(player).getActualRoom().getCenter() == null) return;
            if (!guiManager.getNavManager().getNavData(player).getActualRoom().isInRoom(player.getLocation())){
                player.sendMessage(Decorator.convertError("L'entrée doit se situer dans le rayon d'action de la pièce !"));
                guiManager.getNavManager().closeInventory(player);
                guiManager.getNavManager().messageReopenGUI(player);
            }
            else {
                guiManager.getNavManager().getNavData(player).getActualRoom().setEntry(player.getLocation());
            }
            guiManager.getNavManager().refreshGUI(player);
        }));
    }

    private void createSetCenterButton(Player player, InventoryContents contents) {
        ItemStack icon;
        if (guiManager.getNavManager().getNavData(player).getActualRoom().getCenter() != null){
            Location center = guiManager.getNavManager().getNavData(player).getActualRoom().getCenter();
            icon = ItemUtils.makeGlowItemWithDescription(Material.DIAMOND_ORE,
                    ChatColor.BLUE + "Centre",
                    List.of(ChatColor.GRAY + "Cliques pour mettre le",
                            ChatColor.GRAY + "centre de la pièce",
                            ChatColor.GRAY + "où tu te trouves",
                            ChatColor.RED + "" + ChatColor.BOLD + "/!\\ Changer le centre de place reset",
                            ChatColor.RED + "" + ChatColor.BOLD + "toutes les données de la pièce",
                            ChatColor.RED + "" + ChatColor.BOLD + "qui ont un rapport avec le centre",
                            ChatColor.GRAY + "X: " + ChatColor.WHITE + center.getX(),
                            ChatColor.GRAY + "Y: " + ChatColor.WHITE + center.getY(),
                            ChatColor.GRAY + "Z " + ChatColor.WHITE + center.getZ(),
                            ChatColor.GRAY + "World: " + ChatColor.WHITE + center.getWorld()));
        }
        else {
            icon = ItemUtils.makeItemAndDescription(Material.DIAMOND_ORE,
                    ChatColor.BLUE + "Centre",
                    List.of(ChatColor.GRAY + "Cliques pour mettre le",
                            ChatColor.GRAY + "centre de la pièce",
                            ChatColor.GRAY + "où tu te trouves"));
        }
        contents.set(3, 4, ClickableItem.of(icon, action -> {
            guiManager.getNavManager().getNavData(player).getActualRoom().resetRoomLocData();
            guiManager.getNavManager().getNavData(player).getActualRoom().setCenter(player.getLocation());
            guiManager.getNavManager().refreshGUI(player);
        }));
    }

    private void createShowSizeButton(Player player, InventoryContents contents) {
        contents.set(5, 8, ClickableItem.of(ItemUtils.makeItemAndDescription(Material.SCAFFOLDING,
                ChatColor.GOLD + "Size",
                List.of(ChatColor.WHITE + "Click Gauche " + ChatColor.GRAY + "-> Montrer la taille de la salle",
                        ChatColor.WHITE + "Click Droit " + ChatColor.GRAY + "-> Cacher la taille de la salle")),
                action -> {
                    if (action.isLeftClick()){
                        RPGDjRoom rpgDjRoom = guiManager.getNavManager().getNavData(player).getActualRoom();
                        if (rpgDjRoom.getCenter() == null) return;
                        int sphere = Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, () -> {
                            GeometryUtils.makeSphere(guiManager.getNavManager().getNavData(player).getActualRoom().getCenter(),
                                    guiManager.getNavManager().getNavData(player).getActualRoom().getRoomSize(),
                                    spherePos -> player.spawnParticle(Particle.COMPOSTER, spherePos, 1));
                        }, 0L, 4L);
                        if (!sphereTasks.containsKey(player.getUniqueId()))
                            sphereTasks.put(player.getUniqueId(), sphere);
                        guiManager.getNavManager().closeInventory(player);
                        player.sendMessage(Decorator.convert("Taille de la pièce affichée"));
                    }
                    else {
                        Bukkit.getScheduler().cancelTask(sphereTasks.get(player.getUniqueId()));
                        sphereTasks.remove(player.getUniqueId());
                        guiManager.getNavManager().closeInventory(player);
                        player.sendMessage(Decorator.convert("Taille de la pièce cachée"));
                    }
                    guiManager.getNavManager().messageReopenGUI(player);
                }));
    }

    @Override
    public void update(Player player, InventoryContents contents) {

    }

    private void buildDecoration(InventoryContents inventoryContents){
        ItemStack deco = ItemUtils.makeItem(Material.BLACK_STAINED_GLASS_PANE, " ");
        for (int slot : Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 36, 37, 43, 44, 46, 47, 48, 49, 50, 51, 52)){
            inventoryContents.set(slot / 9, slot % 9, ClickableItem.empty(deco));
        }
    }

    private void createBackButton(Player player, InventoryContents inventoryContents) {
        inventoryContents.set(5, 0, ClickableItem.of(ItemUtils.makeItem(Material.RED_CONCRETE,
                ChatColor.RED + "BACK"), action -> {
            guiManager.getNavManager().openInventory(player, guiManager.getDjSelectRoomsGUI());
        }));
    }
}
