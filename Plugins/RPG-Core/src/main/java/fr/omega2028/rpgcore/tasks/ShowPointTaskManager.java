package fr.omega2028.rpgcore.tasks;

import fr.omega2028.rpgcore.model.RPGCatalogManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Map;

public abstract class ShowPointTaskManager {
    protected final JavaPlugin plugin;
    protected final RPGCatalogManager rpgCatalogManager;
    protected final Map<Player, Integer> playerShow = new HashMap<>();

    protected ShowPointTaskManager(JavaPlugin plugin, RPGCatalogManager rpgCatalogManager){
        this.plugin = plugin;
        this.rpgCatalogManager = rpgCatalogManager;
    }

    public abstract void showPoints(Player player);

    public void hidePoints(Player player){
        Bukkit.getScheduler().cancelTask(playerShow.get(player));
        playerShow.remove(player);
    }
}
