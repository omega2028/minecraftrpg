package fr.omega2028.rpgcore.model.items;

import fr.omega2028.rpgcore.model.items.comportement.Comportement;
import org.bukkit.Material;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class RPGItem {
    private ItemStack baseItem;
    private Rarety rarety;
    private int lvlRequired;
    private final List<Comportement> comportements = new ArrayList<>();

    /**
     * Logic Fields
     */
    private final String rpgID;

    public RPGItem(String rpgID) {
        this.rpgID = rpgID;
        rarety = Rarety.COMMON;
        baseItem = new ItemStack(Material.STONE);
        lvlRequired = 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof RPGItem rpgItem)) return false;
        return getRpgID().equals(rpgItem.getRpgID());
    }

    public int getComportementSize(){
        return comportements.size();
    }

    public void executeComportements(PlayerInteractEvent clickEvent){
        for (Comportement comportement : comportements)
            comportement.onClick(clickEvent);
    }

    public void addComportement(Comportement comportement){
        comportements.add(comportement);
    }

    public Rarety getRarety() {
        return rarety;
    }

    public void setRarety(Rarety rarety) {
        this.rarety = rarety;
    }

    public String getRpgID() {
        return rpgID;
    }

    public ItemStack getBaseItem() {
        return baseItem.clone();
    }

    public void setBaseItem(ItemStack baseItem) {
        this.baseItem = baseItem;
    }

    public int getLvlRequired() {
        return lvlRequired;
    }

    public void setLvlRequired(int lvlRequired) {
        if (lvlRequired < 0) this.lvlRequired = 0;
        else this.lvlRequired = lvlRequired;
    }

    @Override
    public String toString() {
        return "RPGItem{" +
                "baseItem=" + baseItem +
                ", rarety=" + rarety +
                ", rpgID='" + rpgID + '\'' +
                '}';
    }
}
