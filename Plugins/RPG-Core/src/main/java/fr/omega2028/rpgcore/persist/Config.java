package fr.omega2028.rpgcore.persist;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class Config {

        private File configFile;
        private FileConfiguration rcConf;

        public Config(JavaPlugin javaPlugin, String path, String name){
            configFile = new File(javaPlugin.getDataFolder(), path + name + ".yml");
            if(!configFile.exists()){
                try {
                    configFile.createNewFile();
                } catch (Exception e){
                    System.out.println("Error creating Usersfile: " + e);
                }
            }
            rcConf = YamlConfiguration.loadConfiguration(configFile);
        }

        public FileConfiguration getEditableConfigFile(){
            return rcConf;
        }

        public void save(){
            try {
                rcConf.save(configFile);
            } catch (Exception e){
                System.out.println("Error saving Usersfile: " + e);
            }
        }

        public void reload(){
            rcConf = YamlConfiguration.loadConfiguration(configFile);
        }
}
