package fr.omega2028.rpgcore.listeners;

import fr.omega2028.rpgcore.model.NameSpaceKeyManager;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class OnEntityDamaged implements Listener {

    @EventHandler
    public void onEntityDamaged(EntityDamageEvent event){
        if (!(event.getEntity() instanceof LivingEntity livingEntity)) return;
        if (!livingEntity.getPersistentDataContainer().has(NameSpaceKeyManager.getRPGMonsterKey())) return;

    }
}
