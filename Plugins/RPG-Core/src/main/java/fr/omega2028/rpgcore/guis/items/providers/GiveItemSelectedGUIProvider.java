package fr.omega2028.rpgcore.guis.items.providers;

import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.content.InventoryContents;
import fr.minuskube.inv.content.InventoryProvider;
import fr.minuskube.inv.content.Pagination;
import fr.minuskube.inv.content.SlotIterator;
import fr.omega2028.rpgcore.guis.navigation.GUIManager;
import fr.omega2028.rpgcore.model.NameSpaceKeyManager;
import fr.omega2028.rpgcore.model.RPGCatalogManager;
import fr.omega2028.rpgcore.model.items.RPGItem;
import fr.omega2028.rpgcore.model.items.RPGItemUtils;
import fr.omega2028.rpgcore.utils.ItemUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class GiveItemSelectedGUIProvider implements InventoryProvider {
    private final GUIManager guiManager;
    private final RPGCatalogManager rpgCatalogManager;

    public GiveItemSelectedGUIProvider(GUIManager guiManager, RPGCatalogManager rpgCatalogManager) {
        this.guiManager = guiManager;
        this.rpgCatalogManager = rpgCatalogManager;
    }


    @Override
    public void init(Player player, InventoryContents contents) {
        Pagination pagination = contents.pagination();
        buildDecoration(contents);
        createQuitButton(player, contents);
        fillPagination(pagination, player, contents);
        createNavButtons(contents, pagination, player);
        createConvertItemButton(contents, player);
    }

    private void createConvertItemButton(InventoryContents contents, Player player) {
        contents.set(5, 8, ClickableItem.of(ItemUtils.makeItemAndDescription(Material.HOPPER,
                ChatColor.GRAY + "Convertir", List.of(ChatColor.WHITE + "Convertit un item normal",
                        ChatColor.WHITE + "en RPG Item")), action -> {
            ItemStack itemStack = action.getCursor();
            if (itemStack == null || itemStack.getType() == Material.AIR) return;
            UUID uuid = UUID.randomUUID();
            if(itemStack.getItemMeta().getPersistentDataContainer().has(NameSpaceKeyManager.getRPGItemKey())) return;
            rpgCatalogManager.getItemCatalog().put(uuid.toString(), RPGItemUtils.convertItemStackToRPGItem(
                    itemStack, uuid.toString()));
            guiManager.getNavManager().refreshGUI(player);
        }));
    }

    @Override
    public void update(Player player, InventoryContents contents) {

    }

    private void buildDecoration(InventoryContents inventoryContents){
        ItemStack deco = ItemUtils.makeItem(Material.BLACK_STAINED_GLASS_PANE, " ");
        for (int slot : Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 36, 37, 43, 44, 46, 47, 49, 51, 52)){
            inventoryContents.set(slot / 9, slot % 9, ClickableItem.empty(deco));
        }
    }

    private void createQuitButton(Player player, InventoryContents inventoryContents) {
        inventoryContents.set(5, 0, ClickableItem.of(ItemUtils.makeItem(Material.RED_WOOL,
                ChatColor.RED + "QUIT"), action -> {
            guiManager.getNavManager().closeInventory(player);
        }));
    }



    private void fillPagination(Pagination pagination, Player player, InventoryContents inventoryContents) {
        ClickableItem[] items = new ClickableItem[rpgCatalogManager.getItemCatalog().values().size()];
        int index = 0;
        for (RPGItem rpgItem : rpgCatalogManager.getItemCatalog().values()){
            items[index] = ClickableItem.of(RPGItemUtils.buildDisplayRPGItem(rpgItem), action -> {
                if (action.getClick().isLeftClick()) {
                    player.getInventory().addItem(RPGItemUtils.buildRPGItem(rpgItem));
                    guiManager.getNavManager().closeInventory(player);
                }
                else {
                    if (rpgItem.getComportementSize() > 0) return;
                    guiManager.getNavManager().getNavData(player).setActualItem(rpgItem);
                    guiManager.getNavManager().openInventory(player, guiManager.getEditItemGUI());
                }
            });
            index++;
        }
        pagination.setItems(items);
        pagination.setItemsPerPage(27);
        pagination.addToIterator(inventoryContents.newIterator(SlotIterator.Type.HORIZONTAL, 1, 0));
    }

    private void createNavButtons(InventoryContents contents, Pagination pagination, Player player) {
        contents.set(5, 3, ClickableItem.of(ItemUtils.makeItem(Material.ARROW,
                ChatColor.WHITE + "Previous"),
                e -> guiManager.getGiveItemSelectedGUI().open(player, pagination.previous().getPage())));
        contents.set(5, 5, ClickableItem.of(ItemUtils.makeItem(Material.ARROW,
                ChatColor.WHITE + "Next"),
                e -> guiManager.getGiveItemSelectedGUI().open(player, pagination.next().getPage())));
    }
}
