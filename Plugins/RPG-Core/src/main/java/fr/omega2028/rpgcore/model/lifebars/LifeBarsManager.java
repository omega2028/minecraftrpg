package fr.omega2028.rpgcore.model.lifebars;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class LifeBarsManager {
    public Map<UUID, LifeBar> lifeBars = new HashMap<>();

    public void addLifeBar(UUID monsterID, LifeBar lifeBar){
        lifeBars.put(monsterID, lifeBar);
    }

    public void removeLifeBar(UUID monsterID){
        lifeBars.remove(monsterID);
    }
}
