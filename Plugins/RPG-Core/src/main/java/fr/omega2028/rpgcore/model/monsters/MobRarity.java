package fr.omega2028.rpgcore.model.monsters;

import org.bukkit.ChatColor;

public enum MobRarity {
    NORMAL(""),
    SILVER(ChatColor.DARK_GRAY + "" + ChatColor.BOLD + "[Silver]"),
    ELITE(ChatColor.GOLD + "" + ChatColor.BOLD + "[Elite]"),
    BOSS(ChatColor.DARK_RED + "" + ChatColor.BOLD + "[Boss]");

    private String label;

    MobRarity(String label) {
        this.label = label;
    }
}
