package fr.omega2028.rpgcore.commands.monsters;

import fr.omega2028.rpgcore.model.RPGCatalogManager;
import fr.omega2028.rpgcore.model.monsters.RPGMonster;
import fr.omega2028.rpgcore.model.spawner.AutomaticRPGSpawnerUtils;
import fr.omega2028.rpgcore.model.spawner.RPGAutomaticSpawner;
import fr.omega2028.rpgcore.tasks.ShowAutoSpTaskManager;
import fr.omega2028.rpgcore.utils.Decorator;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class SpawnerCommand implements CommandExecutor {
    private final RPGCatalogManager rpgCatalogManager;
    private final AutomaticRPGSpawnerUtils automaticRPGSpawnerUtils;
    private final ShowAutoSpTaskManager showAutoSpTaskManager;

    public SpawnerCommand(RPGCatalogManager rpgCatalogManager, AutomaticRPGSpawnerUtils automaticRPGSpawnerUtils,
                          ShowAutoSpTaskManager showAutoSpTaskManager){
        this.rpgCatalogManager = rpgCatalogManager;
        this.automaticRPGSpawnerUtils = automaticRPGSpawnerUtils;
        this.showAutoSpTaskManager = showAutoSpTaskManager;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command,
                             @NotNull String label, @NotNull String[] args) {
        if (!(sender instanceof Player player) || args.length < 2)
            return false;

        switch (args[1]) {
            case "show":
                showAutoSpTaskManager.showPoints(player);
                player.sendMessage(Decorator.convert("RPG spawners are now visible (only for you)"));
                return true;
            case "hide":
                showAutoSpTaskManager.hidePoints(player);
                player.sendMessage(Decorator.convert("RPG spawners hidden"));
                return true;
            case "delete":
                if (args.length < 3) {
                    player.sendMessage(Decorator.convertError(
                            "/rpg spawner delete <spawnerName>"));
                    return true;
                }
                if (!rpgCatalogManager.getAutoSpawnerCatalog().containsKey(args[2])) {
                    player.sendMessage(Decorator.convertError(
                            "This spawner : \"" + args[2] + "\" doesn't exists"));
                    return true;
                }
                rpgCatalogManager.getAutoSpawnerCatalog().remove(args[2]);
                player.sendMessage(Decorator.convert("The spawner \"" + args[2] + "\" was removed"));
                return true;
        }

        if (args.length < 7){
            player.sendMessage(Decorator.convertError(
                    "/rpg spawner <monsterId> <spawnerName> <activationReach> <checkIfCanSpawnReach> <maxMobsInZone>" +
                            " <secondsBetweenSpawn>"));
            return true;
        }
        else if (rpgCatalogManager.getAutoSpawnerCatalog().containsKey(args[2])){
            player.sendMessage(Decorator.convertError("This spawner already exists !"));
            return true;
        }

        RPGMonster spawnerContent = rpgCatalogManager.getMonsterCatalog().get(args[1]);
        if (spawnerContent == null){
            player.sendMessage(Decorator.convertError("This RPG monster doesn't exists !"));
            return true;
        }

        int activationReach, checkIfCanSpawnReach, maxMobsInZone, secondsBetweenSpawn;
        try {
            activationReach = Integer.parseInt(args[3]);
            checkIfCanSpawnReach = Integer.parseInt(args[4]);
            maxMobsInZone = Integer.parseInt(args[5]);
            secondsBetweenSpawn = Integer.parseInt(args[6]);
        }
        catch (NullPointerException e){
            player.sendMessage(Decorator.convertError("One of activationReach, checkIfCanSpawnReach, maxMobsInZone, " +
                    "secondsBetweenSpawn is not an Integer !"));
            return true;
        }

        /*RPGAutomaticSpawner RPGAutomaticSpawner = new RPGAutomaticSpawner(
                args[2], player.getLocation(), spawnerContent, checkIfCanSpawnReach,
                maxMobsInZone, activationReach, secondsBetweenSpawn);
        rpgCatalogManager.getAutoSpawnerCatalog().put(args[2], RPGAutomaticSpawner);
        automaticRPGSpawnerUtils.activateAutoRPGSpawner(RPGAutomaticSpawner);
        player.sendMessage(Decorator.convert("The spawner \"" + args[2] + "\" was successfully created"));*/
        return true;
    }
}
