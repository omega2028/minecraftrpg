package fr.omega2028.rpgcore.model;

import fr.omega2028.rpgcore.model.dungeon.RPGDungeon;
import fr.omega2028.rpgcore.model.items.RPGItem;
import fr.omega2028.rpgcore.model.monsters.RPGMonster;
import fr.omega2028.rpgcore.model.spawner.RPGAutomaticSpawner;

import java.util.HashMap;
import java.util.Map;

public class RPGCatalogManager {
    private final Map<String, RPGMonster> monsterCatalog = new HashMap<>();
    private final Map<String, RPGDungeon> dungeonCatalog = new HashMap<>();
    private final Map<String, RPGAutomaticSpawner> autoSpawnerCatalog = new HashMap<>();
    private final Map<String, RPGItem> itemCatalog = new HashMap<>();

    public Map<String, RPGMonster> getMonsterCatalog() {
        return monsterCatalog;
    }

    public Map<String, RPGDungeon> getDungeonCatalog() {
        return dungeonCatalog;
    }

    public Map<String, RPGAutomaticSpawner> getAutoSpawnerCatalog() {
        return autoSpawnerCatalog;
    }

    public Map<String, RPGItem> getItemCatalog() {
        return itemCatalog;
    }
}
