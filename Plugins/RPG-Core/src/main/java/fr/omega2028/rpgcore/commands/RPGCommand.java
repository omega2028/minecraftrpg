package fr.omega2028.rpgcore.commands;

import fr.omega2028.rpgcore.commands.dongeon.DungeonGUICommand;
import fr.omega2028.rpgcore.commands.dongeon.GUIReOpenCmd;
import fr.omega2028.rpgcore.commands.items.ItemsGUICommand;
import fr.omega2028.rpgcore.commands.monsters.SpawnMobCommand;
import fr.omega2028.rpgcore.commands.monsters.SpawnerCommand;
import fr.omega2028.rpgcore.guis.navigation.GUIManager;
import fr.omega2028.rpgcore.model.RPGCatalogManager;
import fr.omega2028.rpgcore.model.spawner.AutomaticRPGSpawnerUtils;
import fr.omega2028.rpgcore.tasks.ShowAutoSpTaskManager;
import fr.omega2028.rpgcore.utils.Decorator;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

public class RPGCommand implements CommandExecutor {
    private final CommandExecutor spawnMobCommand;
    private final CommandExecutor spawnerCommand;
    private final CommandExecutor dungeonGUICommand;
    private final CommandExecutor guiReOpenCommand;
    private final CommandExecutor itemGUICommand;

    public RPGCommand(RPGCatalogManager rpgCatalogManager, GUIManager guiManager, JavaPlugin plugin){
        spawnMobCommand = new SpawnMobCommand(rpgCatalogManager);
        spawnerCommand = new SpawnerCommand(rpgCatalogManager, new AutomaticRPGSpawnerUtils(plugin),
                new ShowAutoSpTaskManager(plugin, rpgCatalogManager));
        dungeonGUICommand = new DungeonGUICommand(guiManager);
        guiReOpenCommand = new GUIReOpenCmd(guiManager);
        itemGUICommand = new ItemsGUICommand(guiManager);
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command,
                             @NotNull String label, @NotNull String[] args) {
        if (!(sender instanceof Player player)) return false;
        if (args.length < 1) {
            player.sendMessage(Decorator.convertError("/rpg <dj | spawner | spawn | give | block>"));
            return true;
        }
        switch (args[0]) {
            case "spawn":
                return spawnMobCommand.onCommand(sender, command, label, args);
            case "spawner":
                return spawnerCommand.onCommand(sender, command, label, args);
            case "dungeon":
                return dungeonGUICommand.onCommand(sender, command, label, args);
            case "reopengui":
                return guiReOpenCommand.onCommand(sender, command, label, args);
            case "give":
                return itemGUICommand.onCommand(sender, command, label, args);
            default:
                player.sendMessage(Decorator.convertError("Argument inconnu"));
                return true;
        }
    }
}
