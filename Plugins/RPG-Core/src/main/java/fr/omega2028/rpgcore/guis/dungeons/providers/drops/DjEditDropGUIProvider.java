package fr.omega2028.rpgcore.guis.dungeons.providers.drops;

import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.content.InventoryContents;
import fr.minuskube.inv.content.InventoryProvider;
import fr.omega2028.rpgcore.guis.navigation.GUIManager;
import fr.omega2028.rpgcore.model.items.RPGItemUtils;
import fr.omega2028.rpgcore.utils.ItemUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.List;

public class DjEditDropGUIProvider implements InventoryProvider {
    private final GUIManager guiManager;

    public DjEditDropGUIProvider(GUIManager guiManager) {
        this.guiManager = guiManager;
    }

    @Override
    public void init(Player player, InventoryContents contents) {
        buildDecoration(contents);
        createBackButtons(player, contents);
        createChangeDropChanceButton(player, contents);
        createRpgItemSelector(player, contents);
        createDelButton(player, contents);
    }

    private void createDelButton(Player player, InventoryContents contents) {
        contents.set(0, 4, ClickableItem.of(ItemUtils.makeItemAndDescription(Material.BARRIER,
                ChatColor.RED + "Supprimer", List.of(ChatColor.RED + "Supprimer ce drop,",
                        ChatColor.RED + "/!\\ la supression est " + ChatColor.BOLD + "DEFINITIVE")), action -> {
            guiManager.getNavManager().getNavData(player).getActualDj().delDrop(
                    guiManager.getNavManager().getNavData(player).getActualDrop()
            );
            guiManager.getNavManager().openInventory(player, guiManager.getDjSelectDropsGUI());
        }));
    }

    private void createRpgItemSelector(Player player, InventoryContents contents) {
        contents.set(1, 3, ClickableItem.of(RPGItemUtils.buildDisplayRPGItem(guiManager.getNavManager()
                .getNavData(player).getActualDrop().getRpgItem()), action -> {
            guiManager.getNavManager().openInventory(player, guiManager.getDjChooseItemToDrop());
        }));
    }

    private void createChangeDropChanceButton(Player player, InventoryContents contents) {
        contents.set(1, 5, ClickableItem.of(ItemUtils.makeItemAndDescription(Material.COMPARATOR,
                ChatColor.RED + "Chance de drop", List.of(ChatColor.GOLD + ""
                + ChatColor.BOLD + guiManager.getNavManager().getNavData(player).getActualDrop().getDropProba()*100 + "%")),
                action -> {
                    guiManager.getNavManager().openInventory(player, guiManager.getDjEditDropChanceGUI());
                }));
    }

    @Override
    public void update(Player player, InventoryContents contents) {

    }

    private void buildDecoration(InventoryContents inventoryContents){
        ItemStack deco = ItemUtils.makeItem(Material.BLACK_STAINED_GLASS_PANE, " ");
        for (int slot : Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 10, 16, 18, 19, 20, 21, 22, 23, 24, 25, 26)){
            inventoryContents.set(slot / 9, slot % 9, ClickableItem.empty(deco));
        }
    }

    private void createBackButtons(Player player, InventoryContents inventoryContents) {
        inventoryContents.set(1, 0, ClickableItem.of(ItemUtils.makeItem(Material.GREEN_CONCRETE,
                ChatColor.GREEN + "BACK"), action -> {
            guiManager.getNavManager().openInventory(player, guiManager.getDjSelectDropsGUI());
        }));
        inventoryContents.set(1, 8, ClickableItem.of(ItemUtils.makeItem(Material.GREEN_CONCRETE,
                ChatColor.GREEN + "BACK"), action -> {
            guiManager.getNavManager().openInventory(player, guiManager.getDjSelectDropsGUI());
        }));
    }
}
