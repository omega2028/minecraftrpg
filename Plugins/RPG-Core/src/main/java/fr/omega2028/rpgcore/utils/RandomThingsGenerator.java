package fr.omega2028.rpgcore.utils;

import org.bukkit.Location;
import org.bukkit.Particle;

import java.util.Random;

public class RandomThingsGenerator {
    /**
     * generate a random cloud of given particles around the given point
     * @param location
     * @param width
     * @param height
     * @param particle
     * @param count
     * @param nbPoints
     */
    public static void generateRandomCloud(Location location, float width, float height, Particle particle,
                                           int count, int nbPoints){
        Random random = new Random();
        Location rdmPoint;
        for (int i = 0; i < nbPoints; i++){
            rdmPoint = new Location(location.getWorld(),
                    location.getX() + random.nextFloat(-width, width),
                    location.getY() + random.nextFloat(-height, height),
                    location.getZ() + random.nextFloat(-width, width));
            location.getWorld().spawnParticle(particle, rdmPoint, count, 0.1, 0.1, 0.1);
        }
    }
}
