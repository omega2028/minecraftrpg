package fr.omega2028.rpgcore.commands;

import fr.omega2028.rpgcore.model.RPGCatalogManager;
import fr.omega2028.rpgcore.persist.ItemLoader;
import fr.omega2028.rpgcore.persist.ItemSaver;
import fr.omega2028.rpgcore.utils.Decorator;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class PersistTestCmd implements CommandExecutor {
    private final RPGCatalogManager rpgCatalogManager;
    private final ItemSaver itemSaver;
    private final ItemLoader itemLoader;

    public PersistTestCmd(RPGCatalogManager rpgCatalogManager, ItemSaver itemSaver, ItemLoader itemLoader) {
        this.rpgCatalogManager = rpgCatalogManager;
        this.itemSaver = itemSaver;
        this.itemLoader = itemLoader;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command,
                             @NotNull String label, @NotNull String[] args) {
        if (!(sender instanceof Player player)) return false;
        if (args.length < 1) return false;
        switch (args[0]){
            case "save":
                itemSaver.save(rpgCatalogManager);
                player.sendMessage(Decorator.convert("Data saved."));
                return true;
            case "load":
                itemLoader.load(rpgCatalogManager);
                player.sendMessage(Decorator.convert("Data loaded."));
                return true;
            default:
                return false;
        }
    }
}
