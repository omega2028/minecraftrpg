package fr.omega2028.rpgcore.model.dungeon;

public enum DjDifficulty {
    EASY,
    MEDIUM,
    HARD,
    EXTREME,
    IMPOSSIBLE
}
