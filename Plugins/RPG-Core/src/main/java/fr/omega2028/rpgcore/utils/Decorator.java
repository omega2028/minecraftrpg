package fr.omega2028.rpgcore.utils;

import fr.omega2028.rpgcore.model.items.Rarety;
import org.bukkit.ChatColor;
import org.bukkit.enchantments.Enchantment;

public class Decorator {
    public static String convert(String message){
        return ChatColor.BOLD + "" + ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "RPG-Core"
                + ChatColor.DARK_GRAY + "] " + ChatColor.WHITE + message;
    }

    public static String convertError(String message){
        return ChatColor.BOLD + "" + ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "RPG-Core"
                + ChatColor.DARK_GRAY + "] " + ChatColor.RED + message;
    }

    public static String getSeparator(){
        return ChatColor.WHITE + "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~";
    }

    public static String convertRarety(Rarety rarety) {
        return switch (rarety) {
            case COMMON -> ChatColor.DARK_GRAY + "" + ChatColor.BOLD + "[Commun]";
            case UNCOMMON -> ChatColor.GRAY + "" + ChatColor.BOLD + "[Pas commun]";
            case RARE -> ChatColor.BLUE + "" + ChatColor.BOLD + "[Rare]";
            case EPIC -> ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + "[Epique]";
            case LEGENDARY -> ChatColor.GOLD + "" + ChatColor.BOLD + "[Légendaire]";
            case ANACRONIC -> ChatColor.GRAY + "" + ChatColor.BOLD + "" + ChatColor.MAGIC + "ttt"
                    + ChatColor.GRAY + "" + ChatColor.BOLD + "["
                    + ChatColor.DARK_RED + "" + ChatColor.BOLD + "A"
                    + ChatColor.RED + "" + ChatColor.BOLD + "n"
                    + ChatColor.GOLD + "" + ChatColor.BOLD + "a"
                    + ChatColor.YELLOW + "" + ChatColor.BOLD + "c"
                    + ChatColor.DARK_GREEN + "" + ChatColor.BOLD + "r"
                    + ChatColor.GREEN + "" + ChatColor.BOLD + "o"
                    + ChatColor.AQUA + "" + ChatColor.BOLD + "n"
                    + ChatColor.DARK_AQUA + "" + ChatColor.BOLD + "i"
                    + ChatColor.BLUE + "" + ChatColor.BOLD + "q"
                    + ChatColor.DARK_BLUE + "" + ChatColor.BOLD + "u"
                    + ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "e"
                    + ChatColor.GRAY + "" + ChatColor.BOLD + "]"
                    + ChatColor.GRAY + "" + ChatColor.BOLD + "" + ChatColor.MAGIC + "ttt";
            case ADMIN -> ChatColor.RED + "" + ChatColor.BOLD + "[ADMIN]";
        };
    }

    public static String enchantmentName(String arg) {
        return switch (Enchantment.getByName(arg).getName()) {
            case "ARROW_DAMAGE" -> "Power";
            case "ARROW_FIRE" -> "Flame";
            case "ARROW_INFINITE" -> "Infinity";
            case "ARROW_KNOCKBACK" -> "Punch";
            case "BINDING_CURSE" -> "Curse of Binding";
            case "DAMAGE_ALL" -> "Sharpness";
            case "DAMAGE_ARTHROPODS" -> "Bane of Arthropods";
            case "DAMAGE_UNDEAD" -> "Smite";
            case "DEPTH_STRIDER" -> "Depth Strider";
            case "DIG_SPEED" -> "Efficiency";
            case "DURABILITY" -> "Unbreaking";
            case "FIRE_ASPECT" -> "Fire Aspect";
            case "FROST_WALKER" -> "Frost Walker";
            case "KNOCKBACK" -> "Knockback";
            case "LOOT_BONUS_BLOCKS" -> "Fortune";
            case "LOOT_BONUS_MOBS" -> "Looting";
            case "LUCK" -> "Luck of the Sea";
            case "LURE" -> "Lure";
            case "MENDING" -> "Mending";
            case "OXYGEN" -> "Respiration";
            case "PROTECTION_ENVIRONMENTAL    " -> "Protection";
            case "PROTECTION_EXPLOSIONS" -> "Blast Protection";
            case "PROTECTION_FALL" -> "Feather Falling";
            case "PROTECTION_FIRE" -> "Fire Protection";
            case "PROTECTION_PROJECTILE" -> "Projectile Protection";
            case "SILK_TOUCH" -> "Silk Touch";
            case "SWEEPING_EDGE" -> "Sweeping Edge";
            case "THORNS" -> "Thorns";
            case "VANISHING_CURSE" -> "Cure of Vanishing";
            case "WATER_WORKER" -> "Aqua Affinity";
            default -> "Unknown : " + arg;
        };
    }
}
