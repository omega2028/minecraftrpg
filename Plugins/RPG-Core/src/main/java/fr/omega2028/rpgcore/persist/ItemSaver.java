package fr.omega2028.rpgcore.persist;

import fr.omega2028.rpgcore.model.RPGCatalogManager;
import fr.omega2028.rpgcore.model.items.RPGItem;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ItemSaver{
    private final JavaPlugin plugin;
    private final String itmPath = "rpgitems.";
    private final Config itmConfig;

    public ItemSaver(JavaPlugin javaPlugin) {
        this.plugin = javaPlugin;
        itmConfig = new Config(plugin, "Items/", "rpgitems");
    }

    public void save(RPGCatalogManager rpgCatalogManager) {
        FileConfiguration configFile = itmConfig.getEditableConfigFile();
        Map<String, RPGItem> dataToSave = rpgCatalogManager.getItemCatalog();
        configFile.set(itmPath + "ids", new ArrayList(dataToSave.keySet()));
        for (String key : dataToSave.keySet()){
            if (dataToSave.get(key).getComportementSize() > 0) continue;

            configFile.set(itmPath + key + ".item", dataToSave.get(key).getBaseItem());
            configFile.set(itmPath + key + ".rarety", dataToSave.get(key).getRarety().toString());
            configFile.set(itmPath + key + ".levelRequired", dataToSave.get(key).getLvlRequired());
        }
        itmConfig.save();
    }
}
