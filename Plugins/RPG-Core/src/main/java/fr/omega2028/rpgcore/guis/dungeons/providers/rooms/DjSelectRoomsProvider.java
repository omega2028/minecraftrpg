package fr.omega2028.rpgcore.guis.dungeons.providers.rooms;

import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.content.InventoryContents;
import fr.minuskube.inv.content.InventoryProvider;
import fr.minuskube.inv.content.Pagination;
import fr.minuskube.inv.content.SlotIterator;
import fr.omega2028.rpgcore.guis.navigation.GUIManager;
import fr.omega2028.rpgcore.model.dungeon.RPGDjRoom;
import fr.omega2028.rpgcore.utils.ItemUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.UUID;

public class DjSelectRoomsProvider implements InventoryProvider {
    private final GUIManager guiManager;

    public DjSelectRoomsProvider(GUIManager guiManager) {
        this.guiManager = guiManager;
    }

    @Override
    public void init(Player player, InventoryContents contents) {
        Pagination pagination = contents.pagination();
        buildDecoration(contents);
        createBackButton(player, contents);
        fillDjPagination(pagination, player, contents);
        createNavButtons(contents, pagination, player);
        createAddButton(player, contents);
    }

    @Override
    public void update(Player player, InventoryContents contents) {

    }

    private void createAddButton(Player player, InventoryContents inventoryContents) {
        inventoryContents.set(5, 8, ClickableItem.of(ItemUtils.makeItem(Material.LECTERN,
                ChatColor.WHITE + "Ajouter une nouvelle room"), action -> {
            UUID uuid = UUID.randomUUID();
            guiManager.getNavManager().getNavData(player).getActualDj().addRoom(new RPGDjRoom(uuid.toString()));
            guiManager.getNavManager().refreshGUI(player);
        }));
    }

    private void createNavButtons(InventoryContents contents, Pagination pagination, Player player) {
        contents.set(5, 3, ClickableItem.of(ItemUtils.makeItem(Material.ARROW,
                ChatColor.WHITE + "Previous"),
                e -> guiManager.getDjSelectRoomsGUI().open(player, pagination.previous().getPage())));
        contents.set(5, 5, ClickableItem.of(ItemUtils.makeItem(Material.ARROW,
                ChatColor.WHITE + "Next"),
                e -> guiManager.getDjSelectRoomsGUI().open(player, pagination.next().getPage())));
    }

    private void fillDjPagination(Pagination pagination, Player player, InventoryContents inventoryContents) {
        ClickableItem[] items = new ClickableItem[guiManager.getNavManager().getNavData(player)
                .getActualDj().getRooms().size()];
        int index = 0;
        for (RPGDjRoom rpgDjRoom : guiManager.getNavManager().getNavData(player).getActualDj().getRooms()){
            items[index] = ClickableItem.of(rpgDjRoom.getIcon(), action -> {
                guiManager.getNavManager().getNavData(player).setActualRoom(rpgDjRoom);
                guiManager.getNavManager().openInventory(player, guiManager.getDjEditRoomProvider());
            });
            index++;
        }
        pagination.setItems(items);
        pagination.setItemsPerPage(27);
        pagination.addToIterator(inventoryContents.newIterator(SlotIterator.Type.HORIZONTAL, 1, 0));
    }

    private void buildDecoration(InventoryContents inventoryContents){
        ItemStack deco = ItemUtils.makeItem(Material.BLACK_STAINED_GLASS_PANE, " ");
        for (int slot : Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 36, 37, 43, 44, 46, 47, 49, 51, 52)){
            inventoryContents.set(slot / 9, slot % 9, ClickableItem.empty(deco));
        }
    }

    private void createBackButton(Player player, InventoryContents inventoryContents) {
        inventoryContents.set(5, 0, ClickableItem.of(ItemUtils.makeItem(Material.RED_CONCRETE,
                ChatColor.RED + "BACK"), action -> {
            guiManager.getNavManager().openInventory(player, guiManager.getDjEditGUI());
        }));
    }
}
