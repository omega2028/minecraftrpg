package fr.omega2028.rpgcore.model.monsters;

import fr.omega2028.rpgcore.model.NameSpaceKeyManager;
import fr.omega2028.rpgcore.utils.RandomThingsGenerator;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;

public class RPGMonsterBuilder {
    /**
     * build a RPG monster
     * @param spawnLoc location were will spawn the monster
     * @param rpgMonster monsterData
     * @return the builded monster
     */
    public static LivingEntity buildRPGMonster(Location spawnLoc, RPGMonster rpgMonster, boolean enableSpawnParticle){
        Entity monster = spawnLoc.getWorld().spawnEntity(spawnLoc, rpgMonster.getEntityType(), false);
        if (!(monster instanceof LivingEntity builded))
            throw new IllegalArgumentException("The given EntityType doesn't reprensents a LivingEntity");

        setGraphicalAttributes(builded, rpgMonster);
        setLivingAttributes(builded, rpgMonster);
        setLogicalParameters(builded, rpgMonster);
        setEquipement(builded, rpgMonster);
        if (enableSpawnParticle)
            createParticleEffect(spawnLoc);
        return builded;
    }

    /**
     * set the graphical attributes of the monster
     * @param builded
     * @param rpgMonster
     */
    private static void setGraphicalAttributes(LivingEntity builded, RPGMonster rpgMonster){
        builded.setCustomName(rpgMonster.toString());
        builded.setCustomNameVisible(true);
    }

    /**
     * set the attributes relative to LivingEntity
     * @param builded
     * @param rpgMonster
     */
    private static void setLivingAttributes(LivingEntity builded, RPGMonster rpgMonster){
        builded.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(rpgMonster.getHp());
        builded.setHealth(rpgMonster.getHp());
        builded.getAttribute(Attribute.GENERIC_ATTACK_DAMAGE).setBaseValue(rpgMonster.getBareHandDmg());
    }

    /**
     * set the attributes relative to the logic
     * @param builded
     * @param rpgMonster
     */
    private static void setLogicalParameters(LivingEntity builded, RPGMonster rpgMonster){
        builded.getPersistentDataContainer().set(NameSpaceKeyManager.getRPGMonsterKey(), PersistentDataType.STRING,
                rpgMonster.getMonsterID());
    }

    /**
     * set the equipement of the monster
     * @param builded
     * @param rpgMonster
     */
    private static void setEquipement(LivingEntity builded, RPGMonster rpgMonster){
        if (rpgMonster.getHelmet() != null) {
            builded.getEquipment().setHelmet(editUnbreakable(rpgMonster.getHelmet()));
            builded.getEquipment().setHelmetDropChance(0f);
        }
        if (rpgMonster.getChestplate() != null) {
            builded.getEquipment().setChestplate(editUnbreakable(rpgMonster.getChestplate()));
            builded.getEquipment().setChestplateDropChance(0f);
        }
        if (rpgMonster.getLeggings() != null) {
            builded.getEquipment().setLeggings(editUnbreakable(rpgMonster.getLeggings()));
            builded.getEquipment().setLeggingsDropChance(0f);
        }
        if (rpgMonster.getBoots() != null) {
            builded.getEquipment().setBoots(editUnbreakable(rpgMonster.getBoots()));
            builded.getEquipment().setBootsDropChance(0f);
        }
        if (rpgMonster.getMainHand() != null) {
            builded.getEquipment().setItemInMainHand(editUnbreakable(rpgMonster.getMainHand()));
            builded.getEquipment().setItemInMainHandDropChance(0f);
        }
        if (rpgMonster.getOffHand() != null) {
            builded.getEquipment().setItemInOffHand(editUnbreakable(rpgMonster.getOffHand()));
            builded.getEquipment().setItemInOffHandDropChance(0f);
        }
    }

    /**
     * create a particle effect around a location
     * @param particleLoc
     */
    private static void createParticleEffect(Location particleLoc){
        RandomThingsGenerator.generateRandomCloud(particleLoc, 1, 1, Particle.VILLAGER_ANGRY,
                1, 10);
    }

    /**
     * transform normal item to unbreakable item
     * @param itemStack
     * @return
     */
    private static ItemStack editUnbreakable(ItemStack itemStack){
        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.setUnbreakable(true);
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }
}
