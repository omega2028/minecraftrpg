package fr.omega2028.rpgcore.model.dungeon;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * The data bundle of a Dungeon
 */
public class RPGDungeon {
    /**
     * Logic Fields
     */
    private final String djID;

    /**
     * Fields Métier
     */
    private Location entry;
    private Location exit;
    private ItemStack itemNeededToEnter;
    private boolean isRandom;
    private String name;
    private int RpgXpGivenAtTheEnd = 0;
    private DjDifficulty difficulty = DjDifficulty.EASY;
    private final List<RPGDjRoom> rooms = new LinkedList<>();
    private final List<Drop> lootTable = new ArrayList<>();

    /**
     * GUI Fields
     */
    private ItemStack icon;

    public RPGDungeon(String djID){
        this.djID = djID;
        icon = new ItemStack(Material.GLOW_ITEM_FRAME);
        isRandom = false;
        name = "DefaultName";
    }

    public void addRoom(RPGDjRoom rpgDjRoom){
        rooms.add(rpgDjRoom);
    }

    public void delRoom(RPGDjRoom rpgDjRoom){
        rooms.remove(rpgDjRoom);
    }

    public List<RPGDjRoom> getRooms() {
        return Collections.unmodifiableList(rooms);
    }

    public List<Drop> getLootTable() {
        return Collections.unmodifiableList(lootTable);
    }

    public void delDrop(Drop drop){
        lootTable.remove(drop);
    }

    public int getDropSize(){
        return lootTable.size();
    }

    public void addDrop(Drop drop){
        lootTable.add(drop);
    }

    public void delRoom(String roomID){
        rooms.remove(roomID);
    }

    public Location getEntry() {
        return entry;
    }

    public void setEntry(Location entry) {
        this.entry = entry;
    }

    public Location getExit() {
        return exit;
    }

    public void setExit(Location exit) {
        this.exit = exit;
    }

    public ItemStack getIcon() {
        return icon;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setIcon(ItemStack icon) {
        this.icon = icon;
    }

    public String getDjID() {
        return djID;
    }

    public boolean isRandom() {
        return isRandom;
    }

    public void setRandom(boolean random) {
        isRandom = random;
    }

    public ItemStack getItemNeededToEnter() {
        return itemNeededToEnter;
    }

    public void setItemNeededToEnter(ItemStack itemNeededToEnter) {
        this.itemNeededToEnter = itemNeededToEnter;
    }

    public int getRpgXpGivenAtTheEnd() {
        return RpgXpGivenAtTheEnd;
    }

    public void setRpgXpGivenAtTheEnd(int rpgXpGivenAtTheEnd) {
        if (rpgXpGivenAtTheEnd < 0) rpgXpGivenAtTheEnd = 0;
        RpgXpGivenAtTheEnd = rpgXpGivenAtTheEnd;
    }

    public DjDifficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(DjDifficulty difficulty) {
        this.difficulty = difficulty;
    }
}
