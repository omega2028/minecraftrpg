package fr.omega2028.rpgcore.model.items.comportement.concrete;

import fr.omega2028.rpgcore.model.items.comportement.Comportement;
import fr.omega2028.rpgcore.utils.GeometryUtils;
import org.bukkit.Particle;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.List;

public class LineWithEffectComportement implements Comportement {
    private final int reach;
    private final int effectReachAroundLine;
    private final Particle particle;
    private final int spaceBetweenParticles;
    private final PotionEffectType potionEffectType;
    private final int duration;
    private final int amplifier;

    public LineWithEffectComportement(int reach,
                                      int effectReachAroundLine,
                                      Particle particle,
                                      int spaceBetweenParticles,
                                      PotionEffectType potionEffectType,
                                      int duration,
                                      int amplifier) {
        this.reach = reach;
        this.effectReachAroundLine = effectReachAroundLine;
        this.particle = particle;
        this.spaceBetweenParticles = spaceBetweenParticles;
        this.potionEffectType = potionEffectType;
        this.duration = duration;
        this.amplifier = amplifier;
    }

    @Override
    public void onClick(PlayerInteractEvent clickEvent) {
        if (!clickEvent.getAction().isRightClick()) return;
        Player player = clickEvent.getPlayer();

        GeometryUtils.makeLine(player.getEyeLocation().add(0, 0.5, 0),
                player.getEyeLocation().add(player.getLocation().getDirection().multiply(reach)), spaceBetweenParticles,
                loc -> {
                    player.getLocation().getWorld().spawnParticle(particle, loc, 1);
                    List<LivingEntity> hitTarget = (List<LivingEntity>) loc.getNearbyLivingEntities(effectReachAroundLine);
                    for (LivingEntity hit : hitTarget){
                        if (hit.getUniqueId() == player.getUniqueId()) continue;
                        hit.addPotionEffect(new PotionEffect(potionEffectType, duration, amplifier, false,
                                true));
                    }
                });
    }
}
