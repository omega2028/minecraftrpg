package fr.omega2028.rpgcore.model.items;

import fr.omega2028.rpgcore.model.NameSpaceKeyManager;
import fr.omega2028.rpgcore.utils.Decorator;
import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;

import java.util.ArrayList;
import java.util.List;

public class RPGItemUtils {
    public static RPGItem convertItemStackToRPGItem(ItemStack givenItemStack, String ItemID){
        RPGItem rpgItem = new RPGItem(ItemID);
        ItemStack itemStack = givenItemStack.clone();
        itemStack.setAmount(1);
        rpgItem.setBaseItem(itemStack);
        return rpgItem;
    }

    public static ItemStack buildRPGItem(RPGItem rpgItem){
        ItemStack baseItem = rpgItem.getBaseItem();
        ItemMeta baseItemMeta = baseItem.getItemMeta();
        createLogicProperties(rpgItem, baseItemMeta);
        createLoreAndEnchants(rpgItem, baseItemMeta);
        baseItem.setItemMeta(baseItemMeta);
        return baseItem;
    }

    public static ItemStack buildDisplayRPGItem(RPGItem rpgItem){
        ItemStack baseItem = rpgItem.getBaseItem();
        ItemMeta baseItemMeta = baseItem.getItemMeta();
        createLogicProperties(rpgItem, baseItemMeta);
        createLoreAndEnchants(rpgItem, baseItemMeta);
        List<String> loreAndID = new ArrayList<>(baseItemMeta.getLore());
        loreAndID.add(ChatColor.BOLD + "" + ChatColor.GOLD + "RPG ID: " + ChatColor.WHITE + rpgItem.getRpgID());
        baseItemMeta.setLore(loreAndID);
        baseItem.setItemMeta(baseItemMeta);
        return baseItem;
    }

    private static void createLoreAndEnchants(RPGItem rpgItem, ItemMeta baseItemMeta) {
        List<String> lore = new ArrayList<>();
        baseItemMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        for(Enchantment enchantment : baseItemMeta.getEnchants().keySet()){
            lore.add(ChatColor.GRAY + StringUtils.capitalize(
                    Decorator.enchantmentName(enchantment.getName())
                    + " " + baseItemMeta.getEnchants().get(enchantment)));
        }
        if (rpgItem.getBaseItem().getItemMeta().hasLore()) {
            lore.add(Decorator.getSeparator());
            lore.addAll(rpgItem.getBaseItem().getLore());
            lore.add(Decorator.getSeparator());
        }
        lore.add(Decorator.convertRarety(rpgItem.getRarety()));
        lore.add(ChatColor.YELLOW + "" + ChatColor.BOLD + "Lvl requis: " + rpgItem.getLvlRequired());
        baseItemMeta.setLore(lore);
    }

    private static void createLogicProperties(RPGItem rpgItem, ItemMeta itemMeta) {
        itemMeta.getPersistentDataContainer().set(NameSpaceKeyManager.getRPGItemKey(), PersistentDataType.STRING,
                rpgItem.getRpgID());
    }
}
