package fr.omega2028.rpgcore.persist;

import fr.omega2028.rpgcore.model.RPGCatalogManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Map;

public abstract class Saver {
    protected final JavaPlugin javaPlugin;

    protected Saver(JavaPlugin javaPlugin) {
        this.javaPlugin = javaPlugin;
    }

    public abstract void save(Map<Object, Object> dataToSave);
}
