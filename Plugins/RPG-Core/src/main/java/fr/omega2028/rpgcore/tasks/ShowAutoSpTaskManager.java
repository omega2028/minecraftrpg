package fr.omega2028.rpgcore.tasks;

import fr.omega2028.rpgcore.model.RPGCatalogManager;
import fr.omega2028.rpgcore.model.spawner.RPGAutomaticSpawner;
import org.bukkit.Bukkit;
import org.bukkit.Particle;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class ShowAutoSpTaskManager extends ShowPointTaskManager{
    public ShowAutoSpTaskManager(JavaPlugin plugin, RPGCatalogManager rpgCatalogManager) {
        super(plugin, rpgCatalogManager);
    }

    @Override
    public void showPoints(Player player) {
        int task = Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, () -> {
            for (RPGAutomaticSpawner autoRPGSpawner : rpgCatalogManager.getAutoSpawnerCatalog().values()) {
                player.spawnParticle(Particle.VILLAGER_ANGRY, autoRPGSpawner.getSpawnLoc(), 5);
            }
        }, 0L, 2L);
        playerShow.put(player, task);
    }
}
