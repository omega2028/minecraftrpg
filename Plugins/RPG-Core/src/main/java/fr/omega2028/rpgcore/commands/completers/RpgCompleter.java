package fr.omega2028.rpgcore.commands.completers;

import fr.omega2028.rpgcore.model.RPGCatalogManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class RpgCompleter implements TabCompleter {

    private final RPGCatalogManager rpgCatalogManager;

    public RpgCompleter(RPGCatalogManager rpgCatalogManager) {
        this.rpgCatalogManager = rpgCatalogManager;
    }

    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command command,
                                                @NotNull String alias, @NotNull String[] args) {
        switch (args.length) {
            case 1:
                return List.of("give", "spawn", "block", "spawner", "dungeon");
            case 2:
                switch (args[0]) {
                    case "dj":
                        List<String> retour = new ArrayList<>(rpgCatalogManager.getDungeonCatalog().keySet());
                        retour.addAll(List.of("create", "delete", "<djID>"));
                        return retour;
                    case "spawn":
                        return new ArrayList<>(rpgCatalogManager.getMonsterCatalog().keySet());
                    case "spawner":
                        List<String> info = new ArrayList<>(rpgCatalogManager.getMonsterCatalog().keySet());
                        info.add("show");
                        info.add("hide");
                        info.add("delete");
                        return info;
                    case "give":
                        return List.of("item", "lootBox");
                    case "block":
                        //tout les blockID
                        break;
                    default:
                        return List.of("");
                }
            case 3:
                switch (args[0]){
                    case "dj":
                        switch (args[1]){
                            case "create":
                                return List.of("");
                            case "delete":
                                return new ArrayList<>(rpgCatalogManager.getDungeonCatalog().keySet());
                            default:
                                return List.of("set", "show", "hide", "room", "spawnPoint");
                        }
                    case "spawn":
                        return List.of("1", "2", "5", "10");
                    case "spawner":
                        switch (args[1]){
                            case "delete":
                                return new ArrayList<>(rpgCatalogManager.getAutoSpawnerCatalog().keySet());
                            case "show":
                            case "hide":
                                return List.of("");
                            default:
                                return List.of("<spawnerID>");
                        }
                    case "give":
                        switch (args[1]){
                            case "item":
                                //<itemID>
                                return List.of("");
                            case "lootBox":
                                //<lootBoxId>
                                return List.of("");
                            default:
                                return List.of("");
                        }
                    default:
                        return List.of("");
                }
            case 4:
                switch (args[0]){
                    case "dj":
                        switch (args[2]){
                            case "room":
                                return List.of("list", "add", "del", "spawnPoint");
                            case "set":
                            case "show":
                            case "hide":
                                return List.of("exit", "entry");
                            default:
                                return List.of("");
                        }
                    case "spawner":
                        return List.of("<activationReach>","10","20","50");
                    case "give":
                        if (args[1].equals("item"))
                            return List.of("1", "8", "16", "32", "64");
                    default:
                        return List.of("");
                }
            case 5:
                switch (args[0]){
                    case "dj":
                        switch (args[2]) {
                            case "room":
                                switch (args[3]){
                                    case "spawnPoint":
                                        return List.of("show", "hide", "add", "del");
                                    case "add":
                                        return List.of("<roomID>");
                                    case "del":
                                    default:
                                        return List.of("");
                                }
                            case "set":
                            case "show":
                            case "hide":
                                return List.of("entry", "exit");
                            default:
                                return List.of("");
                        }
                    case "spawner":
                        return List.of("<checkIfCanSpawnReach>","10","20","50");
                    default:
                        return List.of("");
                }
            case 6:
                switch (args[0]){
                    case "dj":
                        if (args[2].equals("spawnPoint") && args[3].equals("add"))
                            return new ArrayList<>(rpgCatalogManager.getMonsterCatalog().keySet());
                        return List.of("");
                    case "spawner":
                        return List.of("<maxMobsInZone>","1","5","10","20");
                    default:
                        return List.of("");
                }
            case 7:
                switch (args[0]) {
                    case "spawner":
                        return List.of("<secondsBetweenSpawn>", "1", "2", "3", "5", "10");
                    default:
                        return List.of("");
                }
            default:
                return List.of("");
        }
    }
}
