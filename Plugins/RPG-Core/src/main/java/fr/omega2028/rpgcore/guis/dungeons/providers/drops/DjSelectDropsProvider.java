package fr.omega2028.rpgcore.guis.dungeons.providers.drops;

import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.content.InventoryContents;
import fr.minuskube.inv.content.InventoryProvider;
import fr.minuskube.inv.content.Pagination;
import fr.minuskube.inv.content.SlotIterator;
import fr.omega2028.rpgcore.guis.navigation.GUIManager;
import fr.omega2028.rpgcore.model.dungeon.Drop;
import fr.omega2028.rpgcore.model.dungeon.RPGDungeon;
import fr.omega2028.rpgcore.model.items.RPGItem;
import fr.omega2028.rpgcore.model.items.RPGItemUtils;
import fr.omega2028.rpgcore.utils.ItemUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class DjSelectDropsProvider implements InventoryProvider {
    private final GUIManager guiManager;

    public DjSelectDropsProvider(GUIManager guiManager) {
        this.guiManager = guiManager;
    }

    @Override
    public void init(Player player, InventoryContents contents) {
        Pagination pagination = contents.pagination();
        buildDecoration(contents);
        createBackButton(player, contents);
        createAddButton(player, contents);
        fillDjPagination(pagination, player, contents);
        createNavButtons(contents, pagination, player);
    }

    private void createAddButton(Player player, InventoryContents inventoryContents) {
        inventoryContents.set(5, 8, ClickableItem.of(ItemUtils.makeItem(Material.LECTERN,
                ChatColor.WHITE + "Create new drop"), action -> {
            guiManager.getNavManager().getNavData(player).getActualDj().addDrop(new Drop(null, 0f));
            guiManager.getNavManager().refreshGUI(player);
        }));
    }

    private void fillDjPagination(Pagination pagination, Player player, InventoryContents inventoryContents) {
        RPGDungeon rpgDungeon = guiManager.getNavManager().getNavData(player).getActualDj();
        ClickableItem[] items = new ClickableItem[rpgDungeon.getDropSize() + 1];
        int index = 0;
        for (Drop drop : rpgDungeon.getLootTable()){
            ItemStack itemStack = RPGItemUtils.buildDisplayRPGItem(drop.getRpgItem());
            List<String> lore = itemStack.getLore();
            if (lore == null) lore = new ArrayList<>();
            lore.add(ChatColor.GRAY + "Drop Chance : " + ChatColor.GOLD + "" + ChatColor.BOLD +
                    drop.getDropProba()*100 + "%");
            ItemMeta itemMeta = itemStack.getItemMeta();
            itemMeta.setLore(lore);
            itemStack.setItemMeta(itemMeta);

            items[index] = ClickableItem.of(itemStack, action -> {
                guiManager.getNavManager().getNavData(player).setActualDrop(drop);
                guiManager.getNavManager().openInventory(player, guiManager.getDjEditDropGUI());
            });
            index++;
        }

        pagination.setItems(items);
        pagination.setItemsPerPage(27);
        pagination.addToIterator(inventoryContents.newIterator(SlotIterator.Type.HORIZONTAL, 1, 0));
    }

    @Override
    public void update(Player player, InventoryContents contents) {

    }

    private void buildDecoration(InventoryContents inventoryContents){
        ItemStack deco = ItemUtils.makeItem(Material.BLACK_STAINED_GLASS_PANE, " ");
        for (int slot : Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 36, 37, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52)){
            inventoryContents.set(slot / 9, slot % 9, ClickableItem.empty(deco));
        }
    }

    private void createBackButton(Player player, InventoryContents inventoryContents) {
        inventoryContents.set(5, 0, ClickableItem.of(ItemUtils.makeItem(Material.RED_CONCRETE,
                ChatColor.RED + "BACK"), action -> {
            guiManager.getNavManager().openInventory(player, guiManager.getDjEditGUI());
        }));
    }

    private void createNavButtons(InventoryContents contents, Pagination pagination, Player player) {
        contents.set(5, 3, ClickableItem.of(ItemUtils.makeItem(Material.ARROW,
                ChatColor.WHITE + "Previous"),
                e -> guiManager.getDjSelectDropsGUI().open(player, pagination.previous().getPage())));
        contents.set(5, 5, ClickableItem.of(ItemUtils.makeItem(Material.ARROW,
                ChatColor.WHITE + "Next"),
                e -> guiManager.getDjSelectDropsGUI().open(player, pagination.next().getPage())));
    }
}
