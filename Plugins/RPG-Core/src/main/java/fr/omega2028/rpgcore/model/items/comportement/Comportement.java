package fr.omega2028.rpgcore.model.items.comportement;

import org.bukkit.event.player.PlayerInteractEvent;

public interface Comportement {
    void onClick(PlayerInteractEvent clickEvent);
}
