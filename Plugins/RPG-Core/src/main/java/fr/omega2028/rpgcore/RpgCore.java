package fr.omega2028.rpgcore;

import fr.minuskube.inv.InventoryManager;
import fr.omega2028.rpgcore.commands.RPGCommand;
import fr.omega2028.rpgcore.commands.completers.RpgCompleter;
import fr.omega2028.rpgcore.game.StaticItemGenerator;
import fr.omega2028.rpgcore.guis.navigation.GUIManager;
import fr.omega2028.rpgcore.listeners.OnPlayerClickEvent;
import fr.omega2028.rpgcore.model.NameSpaceKeyManager;
import fr.omega2028.rpgcore.model.RPGCatalogManager;
import fr.omega2028.rpgcore.model.items.RPGItem;
import fr.omega2028.rpgcore.model.monsters.RPGMonster;
import fr.omega2028.rpgcore.persist.ItemLoader;
import fr.omega2028.rpgcore.persist.ItemSaver;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.conversations.ConversationFactory;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;
import java.util.UUID;

public final class RpgCore extends JavaPlugin {
    private RPGCatalogManager rpgCatalogManager;
    private GUIManager guiManager;

    @Override
    public void onEnable() {
        // Create the keys
        NameSpaceKeyManager.createNSPKeys(this);
        rpgCatalogManager = new RPGCatalogManager();
        guiManager = new GUIManager(rpgCatalogManager, new InventoryManager(this),
                new ConversationFactory(this), this);
        monsterStub();
        itemStub();
        new ItemLoader(this).load(rpgCatalogManager);
        StaticItemGenerator.generateStaticItems(rpgCatalogManager);
        this.getCommand("rpg").setExecutor(new RPGCommand(rpgCatalogManager, guiManager, this));
        this.getCommand("rpg").setTabCompleter(new RpgCompleter(rpgCatalogManager));
        this.getServer().getPluginManager().registerEvents(new OnPlayerClickEvent(rpgCatalogManager), this);
        Bukkit.getLogger().info("RPG-Core Enabeld");
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
        Bukkit.getLogger().info("RPG-Core Disabeld");
        new ItemSaver(this).save(rpgCatalogManager);
    }

    private void monsterStub(){
        RPGMonster ouckOuck = new RPGMonster("pre-normal-ouck-ouck");
        ouckOuck.setEntityType(EntityType.SKELETON);
        ouckOuck.setName(ChatColor.YELLOW + "Ouck Ouck");
        ouckOuck.setHp(10);
        ouckOuck.setLevel(2);
        ouckOuck.setXpRPGValue(5);
        ouckOuck.setHelmet(new ItemStack(Material.IRON_HELMET), 0.1f);
        ouckOuck.setChestplate(new ItemStack(Material.LEATHER_CHESTPLATE), 0.1f);
        rpgCatalogManager.getMonsterCatalog().put("pre-normal-ouck-ouck", ouckOuck);
    }

    private void itemStub() {
        ItemStack superPick = new ItemStack(Material.DIAMOND_PICKAXE);
        ItemMeta superPickMeta = superPick.getItemMeta();
        superPickMeta.setDisplayName(ChatColor.RED + "" + ChatColor.BOLD + "[ SUPER PICK ]");
        superPickMeta.setLore(List.of(
                ChatColor.DARK_GRAY + "Cette pioche est faite d'un cristal",
                ChatColor.DARK_GRAY + "très pur que l'on trouve près les lacs",
                ChatColor.DARK_GRAY + "de laves, elle a été enchantée par le",
                ChatColor.DARK_GRAY + "maître du jeu " + ChatColor.BOLD + "omega2028" + ChatColor.DARK_GRAY + ".",
                ChatColor.DARK_GRAY + "Attention a ne pas tout casser en la",
                ChatColor.DARK_GRAY + "manipulant !"));
        superPickMeta.addEnchant(Enchantment.DIG_SPEED, 25, true);
        superPickMeta.addEnchant(Enchantment.DURABILITY, 15, true);
        superPickMeta.addEnchant(Enchantment.LOOT_BONUS_BLOCKS, 14, true);
        superPickMeta.addEnchant(Enchantment.ARROW_INFINITE, 1, true);
        superPickMeta.setUnbreakable(true);
        superPickMeta.setCustomModelData(1);
        superPick.setItemMeta(superPickMeta);
        UUID uuid = UUID.randomUUID();
        RPGItem rpgItem = new RPGItem(uuid.toString());
        rpgItem.setBaseItem(superPick);
        rpgCatalogManager.getItemCatalog().put(uuid.toString(), rpgItem);

        ItemStack surperSword = new ItemStack(Material.DIAMOND_SWORD);
        ItemMeta surperSwordMeta = surperSword.getItemMeta();
        surperSwordMeta.setDisplayName(ChatColor.RED + "" + ChatColor.BOLD + "[ SUPER SWORD ]");
        surperSwordMeta.setLore(List.of(
                ChatColor.DARK_GRAY + "Cette épée est faite d'un cristal",
                ChatColor.DARK_GRAY + "très pur que l'on trouve près les lacs",
                ChatColor.DARK_GRAY + "de laves, elle a été enchantée par le",
                ChatColor.DARK_GRAY + "maître du jeu " + ChatColor.BOLD + "omega2028" + ChatColor.DARK_GRAY + ".",
                ChatColor.DARK_GRAY + "Attention a ne pas te couper",
                ChatColor.DARK_GRAY + "manipulant !"));
        surperSwordMeta.addEnchant(Enchantment.DAMAGE_ALL, 16, true);
        surperSwordMeta.addEnchant(Enchantment.LOOT_BONUS_MOBS, 10, true);
        surperSwordMeta.addEnchant(Enchantment.DAMAGE_UNDEAD, 20, true);
        surperSwordMeta.setUnbreakable(true);
        surperSwordMeta.setCustomModelData(1);
        surperSword.setItemMeta(surperSwordMeta);
        uuid = UUID.randomUUID();
        rpgItem = new RPGItem(uuid.toString());
        rpgItem.setBaseItem(surperSword);
        rpgCatalogManager.getItemCatalog().put(uuid.toString(), rpgItem);
    }
}
