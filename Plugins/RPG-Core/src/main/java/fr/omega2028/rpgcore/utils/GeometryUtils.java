package fr.omega2028.rpgcore.utils;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class GeometryUtils {

    public static void makeSphere(Location center, int radius, Consumer<Location> spherePoints){
        int r2=radius*radius;
        for(int X = -radius; X <= radius; X++ ){
            int x2=X*X;
            for(int Y = -radius; Y <= radius; Y++ ){
                int y2=Y*Y;
                for(int Z = -radius; Z <= radius; Z++ )
                    if(x2 + y2 + (Z * Z) <= r2)
                        spherePoints.accept(new Location(center.getWorld(),
                                center.getX() + X,
                                center.getY() +Y,
                                center.getZ() +Z));
            }
        }
    }

    public static void makeLine(Location from, Location to, double space, Consumer<Location> locationConsumer) {
        if (from == null || to == null) {
            return;
        }
        if (!from.getWorld().equals(to.getWorld())) return;

        /*Distance between the two particles*/
        double distance = from.distance(to);

        /* The points as vectors */
        Vector p1 = from.toVector();
        Vector p2 = to.toVector();

        /* Subtract gives you a vector between the points, we multiply by the space*/
        Vector vector = p2.clone().subtract(p1).normalize().multiply(space);

        /*The distance covered*/
        double covered = 0;

        /* We run this code while we haven't covered the distance, we increase the point by the space every time*/
        for (; covered < distance; p1.add(vector)) {
            /*Spawn the particle at the point*/
            locationConsumer.accept(p1.toLocation(from.getWorld()));

            /* We add the space covered */
            covered += space;
        }
    }

    public static void makeCircle(Location center, double angle, double rayon, Consumer<Location> locationConsumer){
        double xPosition;
        double zPosition;
        for(int i = 0; angle * i < 360; i++){
            xPosition = center.clone().getX() + rayon*Math.cos(Math.toRadians(angle)*i);
            zPosition = center.clone().getZ() + rayon*Math.sin(Math.toRadians(angle)*i);
            Location tmp = new Location(center.getWorld(), xPosition, center.clone().getY(), zPosition);
            locationConsumer.accept(tmp);
        }
    }

    public static Location findMiddleOfBlock(Block block){
        Location blockLoc = block.getLocation();
        blockLoc.add(0.5, 0, 0.5);
        return blockLoc;
    }
}
