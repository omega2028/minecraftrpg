package fr.omega2028.rpgcore.model.dungeon;

import fr.omega2028.rpgcore.model.items.RPGItem;
import fr.omega2028.rpgcore.utils.ItemUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

public class Drop implements Comparable{
    private RPGItem rpgItem;
    private float dropProba;

    public Drop(RPGItem rpgItem, float dropProba){
        if (rpgItem == null) {
            UUID uuid = UUID.randomUUID();
            rpgItem = new RPGItem(uuid.toString());
            rpgItem.setBaseItem(ItemUtils.makeItem(Material.STONE, ChatColor.GRAY + "Drop par défaut"));
        }
        this.rpgItem = rpgItem;
        setDropProba(dropProba);
    }

    public RPGItem getRpgItem() {
        return rpgItem;
    }

    public void setRpgItem(RPGItem rpgItem) {
        if (rpgItem == null) return;
        this.rpgItem = rpgItem;
    }

    public float getDropProba() {
        return dropProba;
    }

    public void setDropProba(float dropProba) {
        if (dropProba < 0) this.dropProba = 0;
        else if (dropProba > 1) this.dropProba = 1;
        else this.dropProba = dropProba;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Drop drop)) return false;
        return rpgItem.equals(drop.getRpgItem()) && dropProba == drop.getDropProba();
    }

    @Override
    public int compareTo(@NotNull Object o) {
        if (equals(o)) return 1;
        return 0;
    }
}
