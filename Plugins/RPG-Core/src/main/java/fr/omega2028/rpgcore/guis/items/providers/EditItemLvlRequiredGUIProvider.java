package fr.omega2028.rpgcore.guis.items.providers;

import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.content.InventoryContents;
import fr.minuskube.inv.content.InventoryProvider;
import fr.omega2028.rpgcore.guis.navigation.GUIManager;
import fr.omega2028.rpgcore.utils.ItemUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.List;

public class EditItemLvlRequiredGUIProvider implements InventoryProvider {
    private final GUIManager guiManager;

    public EditItemLvlRequiredGUIProvider(GUIManager guiManager) {
        this.guiManager = guiManager;
    }

    @Override
    public void init(Player player, InventoryContents contents) {
        buildDecoration(contents);
        createSaveBtns(player, contents);
        createLvlRequiredDisplay(player, contents);
        createLvlRequiredModifiers(player, contents);
    }

    @Override
    public void update(Player player, InventoryContents contents) {

    }

    private void createLvlRequiredDisplay(Player player, InventoryContents contents) {
        contents.set(2, 4, ClickableItem.empty(ItemUtils.makeItemAndDescription(Material.EXPERIENCE_BOTTLE,
                ChatColor.WHITE + "Level requis",
                List.of(ChatColor.GOLD + "" + ChatColor.BOLD + "Niveau : "
                        + guiManager.getNavManager().getNavData(player).getActualItem().getLvlRequired()))));
    }

    private void createSaveBtns(Player player, InventoryContents contents) {
        contents.set(3, 8, ClickableItem.of(ItemUtils.makeItem(Material.GREEN_CONCRETE,
                ChatColor.GREEN + "SAVE"), action -> {
            guiManager.getNavManager().openInventory(player, guiManager.getEditItemGUI());
        }));
        contents.set(3, 0, ClickableItem.of(ItemUtils.makeItem(Material.GREEN_CONCRETE,
                ChatColor.GREEN + "SAVE"), action -> {
            guiManager.getNavManager().openInventory(player, guiManager.getEditItemGUI());
        }));
    }

    private void buildDecoration(InventoryContents inventoryContents){
        ItemStack deco = ItemUtils.makeItem(Material.BLACK_STAINED_GLASS_PANE, " ");
        for (int slot : Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 17, 18, 19, 25, 26, 28, 29, 30, 31, 32, 33, 34)){
            inventoryContents.set(slot / 9, slot % 9, ClickableItem.empty(deco));
        }
    }

    private void createLvlRequiredModifiers(Player player, InventoryContents contents) {

        contents.set(1, 1, ClickableItem.of(ItemUtils.makeItem(Material.INK_SAC,
                ChatColor.RED + "" + ChatColor.BOLD + "-10"
                        + ChatColor.BOLD + "" + ChatColor.GOLD + "LVL"), action -> {
            guiManager.getNavManager().getNavData(player).getActualItem().setLvlRequired(
                    guiManager.getNavManager().getNavData(player).getActualItem().getLvlRequired() - 10);
            guiManager.getNavManager().refreshGUI(player);
        }));
        contents.set(1, 2, ClickableItem.of(ItemUtils.makeItem(Material.BLACK_DYE,
                ChatColor.RED + "" + ChatColor.BOLD + "-1"
                        + ChatColor.BOLD + "" + ChatColor.GOLD + "LVL"), action -> {
            guiManager.getNavManager().getNavData(player).getActualItem().setLvlRequired(
                    guiManager.getNavManager().getNavData(player).getActualItem().getLvlRequired() - 1);
            guiManager.getNavManager().refreshGUI(player);
        }));
        contents.set(1, 6, ClickableItem.of(ItemUtils.makeItem(Material.BLACK_DYE,
                ChatColor.RED + "" + ChatColor.BOLD + "+1"
                        + ChatColor.BOLD + "" + ChatColor.GOLD + "LVL"), action -> {
            guiManager.getNavManager().getNavData(player).getActualItem().setLvlRequired(
                    guiManager.getNavManager().getNavData(player).getActualItem().getLvlRequired() + 1);
            guiManager.getNavManager().refreshGUI(player);
        }));
        contents.set(1, 7, ClickableItem.of(ItemUtils.makeItem(Material.BLACK_DYE,
                ChatColor.RED + "" + ChatColor.BOLD + "+10"
                        + ChatColor.BOLD + "" + ChatColor.GOLD + "LVL"), action -> {
            guiManager.getNavManager().getNavData(player).getActualItem().setLvlRequired(
                    guiManager.getNavManager().getNavData(player).getActualItem().getLvlRequired() + 10);
            guiManager.getNavManager().refreshGUI(player);
        }));
    }
}
