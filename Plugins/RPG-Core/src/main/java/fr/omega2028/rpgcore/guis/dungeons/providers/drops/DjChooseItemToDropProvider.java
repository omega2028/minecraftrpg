package fr.omega2028.rpgcore.guis.dungeons.providers.drops;

import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.content.InventoryContents;
import fr.minuskube.inv.content.InventoryProvider;
import fr.minuskube.inv.content.Pagination;
import fr.minuskube.inv.content.SlotIterator;
import fr.omega2028.rpgcore.guis.navigation.GUIManager;
import fr.omega2028.rpgcore.model.RPGCatalogManager;
import fr.omega2028.rpgcore.model.items.RPGItem;
import fr.omega2028.rpgcore.model.items.RPGItemUtils;
import fr.omega2028.rpgcore.utils.ItemUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;

public class DjChooseItemToDropProvider implements InventoryProvider {
    private final GUIManager guiManager;
    private final RPGCatalogManager rpgCatalogManager;

    public DjChooseItemToDropProvider(GUIManager guiManager, RPGCatalogManager rpgCatalogManager) {
        this.guiManager = guiManager;
        this.rpgCatalogManager = rpgCatalogManager;
    }


    @Override
    public void init(Player player, InventoryContents contents) {
        Pagination pagination = contents.pagination();
        buildDecoration(contents);
        createBackButtons(player, contents);
        fillPagination(pagination, player, contents);
        createNavButtons(contents, pagination, player);
    }

    @Override
    public void update(Player player, InventoryContents contents) {

    }

    private void buildDecoration(InventoryContents inventoryContents){
        ItemStack deco = ItemUtils.makeItem(Material.BLACK_STAINED_GLASS_PANE, " ");
        for (int slot : Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 36, 37, 43, 44, 46, 47, 49, 51, 52)){
            inventoryContents.set(slot / 9, slot % 9, ClickableItem.empty(deco));
        }
    }

    private void createBackButtons(Player player, InventoryContents inventoryContents) {
        inventoryContents.set(5, 0, ClickableItem.of(ItemUtils.makeItem(Material.RED_WOOL,
                ChatColor.RED + "BACK"), action -> {
            guiManager.getNavManager().openInventory(player, guiManager.getDjEditDropGUI());
        }));
        inventoryContents.set(5, 8, ClickableItem.of(ItemUtils.makeItem(Material.RED_WOOL,
                ChatColor.RED + "BACK"), action -> {
            guiManager.getNavManager().openInventory(player, guiManager.getDjEditDropGUI());
        }));
    }

    private void fillPagination(Pagination pagination, Player player, InventoryContents inventoryContents) {
        ClickableItem[] items = new ClickableItem[rpgCatalogManager.getItemCatalog().values().size()];
        int index = 0;
        for (RPGItem rpgItem : rpgCatalogManager.getItemCatalog().values()){
            items[index] = ClickableItem.of(RPGItemUtils.buildDisplayRPGItem(rpgItem), action -> {
                guiManager.getNavManager().getNavData(player).getActualDrop().setRpgItem(rpgItem);
                guiManager.getNavManager().openInventory(player, guiManager.getDjEditDropGUI());
            });
            index++;
        }
        pagination.setItems(items);
        pagination.setItemsPerPage(27);
        pagination.addToIterator(inventoryContents.newIterator(SlotIterator.Type.HORIZONTAL, 1, 0));
    }

    private void createNavButtons(InventoryContents contents, Pagination pagination, Player player) {
        contents.set(5, 3, ClickableItem.of(ItemUtils.makeItem(Material.ARROW,
                ChatColor.WHITE + "Previous"),
                e -> guiManager.getDjChooseItemToDrop().open(player, pagination.previous().getPage())));
        contents.set(5, 5, ClickableItem.of(ItemUtils.makeItem(Material.ARROW,
                ChatColor.WHITE + "Next"),
                e -> guiManager.getDjChooseItemToDrop().open(player, pagination.next().getPage())));
    }
}
