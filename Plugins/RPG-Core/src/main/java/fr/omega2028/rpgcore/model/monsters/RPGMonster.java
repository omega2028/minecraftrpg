package fr.omega2028.rpgcore.model.monsters;

import org.bukkit.ChatColor;
import org.bukkit.NamespacedKey;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Store Data of one RPG monster
 */
public class RPGMonster {
    /**
     * Vanilla stats
     */
    private EntityType entityType;
    private String name;
    private int hp;
    private int bareHandDmg;

    /**
     * Rpg stats
     */
    private int level;
    private int XpRPGValue;

    /**
     * RPG Stuff
     */
    private ItemStack helmet;
    private ItemStack chestplate;
    private ItemStack leggings;
    private ItemStack boots;
    private ItemStack rightHand;
    private ItemStack leftHand;
    private float helmetDropChance;
    private float chestplateDropChance;
    private float leggingsDropChance;
    private float bootsDropChance;
    private float leftHandDropChance;
    private float rightHandDropChance;

    /**
     * Logic things
     */
    private final String monsterID;

    /**
     * Constructor of a RPG monster
     */
    public RPGMonster(String monsterID){
        this.monsterID = monsterID;
        entityType = EntityType.PIG;
        name = ChatColor.GRAY + "DefaultName";
        hp = 1;
        bareHandDmg = 1;
        level = 1;
        XpRPGValue = 0;
    }

    @Override
    public String toString(){
        return ChatColor.GRAY + "[" + ChatColor.WHITE + level + ChatColor.GRAY + "] "
                + name;
    }

    public void setHelmet(ItemStack helmet, float helmetDropChance) {
        this.helmet = helmet;
        this.helmetDropChance = helmetDropChance;
    }

    public void setChestplate(ItemStack chestplate, float chestplateDropChance) {
        this.chestplate = chestplate;
        this.chestplateDropChance = chestplateDropChance;
    }

    public void setLeggings(ItemStack leggings, float leggingsDropChance) {
        this.leggings = leggings;
        this.leggingsDropChance = leggingsDropChance;
    }

    public void setBoots(ItemStack boots, float bootsDropChance) {
        this.boots = boots;
        this.bootsDropChance = bootsDropChance;
    }

    public void setRightHand(ItemStack rightHand, float rightHandDropChance) {
        this.rightHand = rightHand;
        this.rightHandDropChance = rightHandDropChance;
    }

    public void setLeftHand(ItemStack leftHand, float leftHandDropChance) {
        this.leftHand = leftHand;
        this.leftHandDropChance = leftHandDropChance;
    }

    public int getBareHandDmg() {
        return bareHandDmg;
    }

    public int getHp() {
        return hp;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public String getMonsterID() {
        return monsterID;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public void setBareHandDmg(int bareHandDmg) {
        this.bareHandDmg = bareHandDmg;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getXpRPGValue() {
        return XpRPGValue;
    }

    public void setXpRPGValue(int xpRPGValue) {
        XpRPGValue = xpRPGValue;
    }

    public ItemStack getHelmet() {
        return helmet;
    }

    public ItemStack getChestplate() {
        return chestplate;
    }

    public ItemStack getLeggings() {
        return leggings;
    }

    public ItemStack getBoots() {
        return boots;
    }

    public ItemStack getMainHand() {
        return rightHand;
    }

    public ItemStack getOffHand() {
        return leftHand;
    }

    public float getHelmetDropChance() {
        return helmetDropChance;
    }

    public float getChestplateDropChance() {
        return chestplateDropChance;
    }

    public float getLeggingsDropChance() {
        return leggingsDropChance;
    }

    public float getBootsDropChance() {
        return bootsDropChance;
    }

    public float getLeftHandDropChance() {
        return leftHandDropChance;
    }

    public float getRightHandDropChance() {
        return rightHandDropChance;
    }
}
