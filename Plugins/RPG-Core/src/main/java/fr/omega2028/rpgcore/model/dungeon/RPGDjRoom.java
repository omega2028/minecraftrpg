package fr.omega2028.rpgcore.model.dungeon;

import fr.omega2028.rpgcore.model.spawner.RPGSpawner;
import fr.omega2028.rpgcore.utils.ItemUtils;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class RPGDjRoom {
    private final String roomID;
    private ItemStack icon;

    private int roomSize;

    private Map<String, RPGSpawner> spawners = new HashMap<>();
    private Location center;
    private Location entry;
    private Location exit;

    public RPGDjRoom(String roomID) {
        this.roomID = roomID;
        icon = ItemUtils.makeItem(Material.BLACK_GLAZED_TERRACOTTA, ChatColor.WHITE + roomID);
        roomSize = 10;
    }

    public void resetRoomLocData() {
        spawners.clear();
        center = null;
        entry = null;
        exit = null;
    }

    public boolean isInRoom(Location location){
        return center.distance(location) <= roomSize;
    }

    public Map<String, RPGSpawner> getSpawners() {
        return Collections.unmodifiableMap(spawners);
    }

    public void addSpawner(String spID, RPGSpawner rpgSpawner){
        spawners.put(spID, rpgSpawner);
    }

    public void delSpawner(String spID){
        spawners.remove(spID);
    }

    public ItemStack getIcon() {
        return icon;
    }

    public Location getCenter() {
        return center;
    }

    public int getRoomSize() {
        return roomSize;
    }

    public void setRoomSize(int roomSize) {
        this.roomSize = roomSize;
    }

    public void setCenter(Location center) {
        this.center = center;
    }

    public String getRoomID() {
        return roomID;
    }

    public Location getEntry() {
        return entry;
    }

    public void setEntry(Location entry) {
        this.entry = entry;
    }

    public Location getExit() {
        return exit;
    }

    public void setExit(Location exit) {
        this.exit = exit;
    }


}
