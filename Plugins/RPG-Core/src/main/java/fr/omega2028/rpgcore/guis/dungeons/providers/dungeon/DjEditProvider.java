package fr.omega2028.rpgcore.guis.dungeons.providers.dungeon;

import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.content.InventoryContents;
import fr.minuskube.inv.content.InventoryProvider;
import fr.omega2028.rpgcore.guis.navigation.GUIManager;
import fr.omega2028.rpgcore.model.RPGCatalogManager;
import fr.omega2028.rpgcore.model.dungeon.DjDifficulty;
import fr.omega2028.rpgcore.tasks.PointIndicator;
import fr.omega2028.rpgcore.utils.Decorator;
import fr.omega2028.rpgcore.utils.ItemUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Arrays;
import java.util.List;

public class DjEditProvider implements InventoryProvider {
    private final RPGCatalogManager rpgCatalogManager;
    private final GUIManager guiManager;
    private final JavaPlugin plugin;
    private final PointIndicator pointIndicator;

    public DjEditProvider(RPGCatalogManager rpgCatalogManager, GUIManager guiManager, JavaPlugin plugin) {
        this.rpgCatalogManager = rpgCatalogManager;
        this.guiManager = guiManager;
        this.plugin = plugin;
        this.pointIndicator = new PointIndicator(plugin);
    }


    @Override
    public void init(Player player, InventoryContents contents) {
        if (guiManager.getNavManager().getNavData(player).getActualDj() == null)
            guiManager.getDjSelectionGUI().open(player);
        buildDecoration(contents);
        createBackButton(player, contents);
        createIcon(player, contents);
        createIsRandomButton(player, contents);
        createDjChangeNameButton(player, contents);
        createItemNeededToEnter(player, contents);
        createXpGivenEndButton(player, contents);
        createDifficulty(player, contents);
        createSetEntryButton(player, contents);
        createSetExitButton(player, contents);
        createDeleteButton(player, contents);
        createEditRoomsButton(player, contents);
        createEditDropsButton(player, contents);
        createShowPointsButton(player, contents);
        createHidePointsButton(player, contents);
    }

    private void createHidePointsButton(Player player, InventoryContents contents) {
        contents.set(0, 5, ClickableItem.of(ItemUtils.makeItem(Material.SPYGLASS,
                ChatColor.GRAY + "Cacher l'entrée et la sortie"), action -> {
            pointIndicator.hidePoints(player);
            guiManager.getNavManager().closeInventory(player);
            guiManager.getNavManager().messageReopenGUI(player);
        }));
    }

    private void createShowPointsButton(Player player, InventoryContents contents) {
        contents.set(0, 3, ClickableItem.of(ItemUtils.makeItem(Material.SPYGLASS,
                ChatColor.GRAY + "Montrer l'entrée et la sortie"), action -> {
            if (guiManager.getNavManager().getNavData(player).getActualDj().getEntry() != null)
                pointIndicator.showPoint(player,
                        guiManager.getNavManager().getNavData(player).getActualDj().getEntry());
            if (guiManager.getNavManager().getNavData(player).getActualDj().getExit() != null)
                pointIndicator.showPoint(player,
                        guiManager.getNavManager().getNavData(player).getActualDj().getExit());
            guiManager.getNavManager().closeInventory(player);
            guiManager.getNavManager().messageReopenGUI(player);
        }));
    }

    private void createEditDropsButton(Player player, InventoryContents contents) {
        contents.set(5, 8, ClickableItem.of(ItemUtils.makeItem(Material.HOPPER,
                ChatColor.GRAY + "Editer les drops"), action -> {
            guiManager.getNavManager().openInventory(player, guiManager.getDjSelectDropsGUI());
        }));
    }

    private void createEditRoomsButton(Player player, InventoryContents contents) {
        contents.set(2, 4, ClickableItem.of(ItemUtils.makeItem(Material.BELL,
                ChatColor.GOLD + "Editer les rooms"), action -> {
            guiManager.getNavManager().openInventory(player, guiManager.getDjSelectRoomsGUI());
        }));
    }

    private void createDeleteButton(Player player, InventoryContents contents) {
        contents.set(0, 4, ClickableItem.of(ItemUtils.makeItemAndDescription(Material.BARRIER,
                ChatColor.RED + "Supprimer", List.of(ChatColor.RED + "Supprime ce donjon,",
                        ChatColor.RED + "/!\\ la supression est " + ChatColor.BOLD + "DEFINITIVE")), action -> {
                    guiManager.getNavManager().openInventory(player, guiManager.getDjDelConfirmGUI());
                }));
    }

    private void createSetExitButton(Player player, InventoryContents contents) {
        ItemStack entry;
        List<String> description = List.of(ChatColor.WHITE + "Set la sortie à l'endroit ou vous vous trouvez");
        if (guiManager.getNavManager().getNavData(player).getActualDj().getExit() == null)
            entry = ItemUtils.makeItemAndDescription(Material.ORANGE_STAINED_GLASS_PANE,
                    ChatColor.GOLD + "Sortie", description);
        else
            entry = ItemUtils.makeItemAndDescription(Material.LIME_STAINED_GLASS_PANE,
                    ChatColor.GREEN + "Sortie", description);
        contents.set(2, 7, ClickableItem.of(entry, action -> {
            guiManager.getNavManager().getNavData(player).getActualDj().setExit(player.getLocation());
            player.sendMessage(Decorator.convert("La sortie a été set à l'endroit ou vous vous trouvez"));
            guiManager.getNavManager().closeInventory(player);
            guiManager.getNavManager().messageReopenGUI(player);
        }));
    }

    private void createSetEntryButton(Player player, InventoryContents contents) {
        ItemStack entry;
        List<String> description = List.of(ChatColor.WHITE + "Set l'entrée à l'endroit ou vous vous trouvez");
        if (guiManager.getNavManager().getNavData(player).getActualDj().getEntry() == null)
            entry = ItemUtils.makeItemAndDescription(Material.ORANGE_STAINED_GLASS_PANE,
                    ChatColor.GOLD + "Entrée", description);
        else
            entry = ItemUtils.makeItemAndDescription(Material.LIME_STAINED_GLASS_PANE,
                    ChatColor.GREEN + "Entrée", description);
        contents.set(2, 1, ClickableItem.of(entry, action -> {
            guiManager.getNavManager().getNavData(player).getActualDj().setEntry(player.getLocation());
            player.sendMessage(Decorator.convert("L'entrée a été set à l'endroit ou vous vous trouvez"));
            guiManager.getNavManager().closeInventory(player);
            guiManager.getNavManager().messageReopenGUI(player);
        }));
    }

    private void createDifficulty(Player player, InventoryContents contents) {
        DjDifficulty difficulty = guiManager.getNavManager().getNavData(player).getActualDj().getDifficulty();
        String difDisplay;
        switch (difficulty){
            case EASY -> difDisplay = ChatColor.GREEN + difficulty.toString();
            case MEDIUM -> difDisplay = ChatColor.GOLD + difficulty.toString();
            case HARD -> difDisplay = ChatColor.RED + difficulty.toString();
            case EXTREME -> difDisplay = ChatColor.DARK_RED + difficulty.toString();
            case IMPOSSIBLE -> difDisplay = ChatColor.DARK_PURPLE + "" + ChatColor.MAGIC + "" + ChatColor.BOLD + "ttt"
                    + ChatColor.DARK_PURPLE + difficulty.toString()
                    + ChatColor.DARK_PURPLE + "" + ChatColor.MAGIC + "" + ChatColor.BOLD + "ttt";
            default -> difDisplay = "WTFF";
        }
        contents.set(4, 6, ClickableItem.of(ItemUtils.makeItemAndDescription(Material.FIRE_CORAL,
                ChatColor.WHITE + "Dungeon's difficulty", List.of(difDisplay)), action -> {
            switch (difficulty){
                case EASY -> guiManager.getNavManager().getNavData(player).getActualDj().setDifficulty(DjDifficulty.MEDIUM);
                case MEDIUM -> guiManager.getNavManager().getNavData(player).getActualDj().setDifficulty(DjDifficulty.HARD);
                case HARD -> guiManager.getNavManager().getNavData(player).getActualDj().setDifficulty(DjDifficulty.EXTREME);
                case EXTREME -> guiManager.getNavManager().getNavData(player).getActualDj().setDifficulty(DjDifficulty.IMPOSSIBLE);
                case IMPOSSIBLE -> guiManager.getNavManager().getNavData(player).getActualDj().setDifficulty(DjDifficulty.EASY);
                default -> guiManager.getNavManager().getNavData(player).getActualDj().setDifficulty(DjDifficulty.EASY);
            }
            guiManager.getNavManager().refreshGUI(player);
        }));
    }

    private void createXpGivenEndButton(Player player, InventoryContents contents) {
        contents.set(4, 5, ClickableItem.of(ItemUtils.makeItemAndDescription(Material.EXPERIENCE_BOTTLE,
                 ChatColor.GOLD + "Exp donnée",
                List.of(ChatColor.WHITE + "Expérience RPG donnée ",
                        ChatColor.WHITE + "à la fin du donjon : ",
                        ChatColor.WHITE + "" + guiManager.getNavManager().getNavData(player).getActualDj()
                .getRpgXpGivenAtTheEnd() + ChatColor.GOLD + " XP")), action -> {
            guiManager.getNavManager().openInventory(player, guiManager.getDjXpGUI());
        }));
    }

    private void createItemNeededToEnter(Player player, InventoryContents contents) {
        ItemStack icon;
        if (guiManager.getNavManager().getNavData(player).getActualDj().getItemNeededToEnter() == null)
            icon = ItemUtils.makeItemAndDescription(
                    Material.TRIPWIRE_HOOK,
                    ChatColor.WHITE + "Item requis pour entrer",
                    List.of(ChatColor.WHITE + "Mets l'item que tu", ChatColor.WHITE + "veux ici"));
        else
            icon = guiManager.getNavManager().getNavData(player).getActualDj().getItemNeededToEnter();
        contents.set(4, 4, ClickableItem.of(icon,
                action -> {
                    if (action.getCursor() == null || action.getCursor().getType() == Material.AIR) return;
                    guiManager.getNavManager().getNavData(player).getActualDj()
                            .setItemNeededToEnter(action.getCursor());
                    action.getWhoClicked().getInventory().addItem(action.getCursor());
                    action.setCursor(new ItemStack(Material.AIR));
                    guiManager.getNavManager().refreshGUI(player);
                }));
    }

    private void createDjChangeNameButton(Player player, InventoryContents contents) {
        String name =  guiManager.getNavManager().getNavData(player).getActualDj().getName();
        contents.set(4, 3, ClickableItem.of(ItemUtils.makeItemAndDescription(Material.NAME_TAG,
                name, List.of(
                        ChatColor.WHITE + "Glisse un item renommé ici pour",
                        ChatColor.WHITE + "changer le nom du donjon",
                        ChatColor.WHITE + "si cet item n'a pas de nom custom",
                        ChatColor.WHITE + "il se passe rien :p")), action -> {
            if (action.getCursor() == null || action.getCursor().getType() == Material.AIR) return;
            ItemStack itmName = action.getCursor();
            if (itmName.getItemMeta().hasDisplayName())
                guiManager.getNavManager().getNavData(player).getActualDj().setName(itmName.getItemMeta().getDisplayName());
            guiManager.getNavManager().refreshGUI(player);
        }));
    }

    private void createIsRandomButton(Player player, InventoryContents contents) {
        String rdm;
        if (guiManager.getNavManager().getNavData(player).getActualDj().isRandom())
            rdm = "Activé";
        else
            rdm = "Désactivé";
        contents.set(4, 2, ClickableItem.of(ItemUtils.makeItemAndDescription(Material.HEART_OF_THE_SEA,
                ChatColor.WHITE + "Donjon random", List.of(ChatColor.WHITE + rdm)), action -> {
            guiManager.getNavManager().getNavData(player).getActualDj()
                    .setRandom(!guiManager.getNavManager().getNavData(player).getActualDj().isRandom());
            guiManager.getNavManager().refreshGUI(player);
        }));
    }

    private void createIcon(Player player, InventoryContents contents) {
        contents.set(5, 4, ClickableItem.of(ItemUtils.makeItemAndDescription(
                guiManager.getNavManager().getNavData(player).getActualDj().getIcon().getType(),
                ChatColor.WHITE + "Icon",
                List.of(ChatColor.WHITE + "Icon, mets l'item que tu veux ici",
                        ChatColor.WHITE + "il servira d'icon pour afficher le donjon")),
                action -> {
                    if (action.getCursor() == null || action.getCursor().getType() == Material.AIR) return;
                    guiManager.getNavManager().getNavData(player).getActualDj().setIcon(action.getCursor());
                    action.getWhoClicked().getInventory().addItem(action.getCursor());
                    action.setCursor(new ItemStack(Material.AIR));
                    guiManager.getNavManager().refreshGUI(player);
                }));
    }

    @Override
    public void update(Player player, InventoryContents contents) {

    }

    private void buildDecoration(InventoryContents inventoryContents){
        ItemStack deco = ItemUtils.makeItem(Material.BLACK_STAINED_GLASS_PANE, " ");
        for (int slot : Arrays.asList(0, 1, 2, 3, 5, 6, 7, 8, 36, 37, 43, 44, 46, 47, 48, 50, 51, 52)){
            inventoryContents.set(slot / 9, slot % 9, ClickableItem.empty(deco));
        }
    }

    private void createBackButton(Player player, InventoryContents inventoryContents) {
        inventoryContents.set(5, 0, ClickableItem.of(ItemUtils.makeItem(Material.RED_CONCRETE,
                ChatColor.RED + "BACK"), action -> {
            guiManager.getNavManager().openInventory(player, guiManager.getDjSelectionGUI());
        }));
    }
}
