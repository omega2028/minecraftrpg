package fr.omega2028.rpgcore.commands.dongeon;

import fr.omega2028.rpgcore.guis.navigation.GUIManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class GUIReOpenCmd implements CommandExecutor {
    private final GUIManager guiManager;

    public GUIReOpenCmd(GUIManager guiManager) {
        this.guiManager = guiManager;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command,
                             @NotNull String label, @NotNull String[] args) {
        if (!(sender instanceof Player player)) return false;
        guiManager.getNavManager().openInventory(player, guiManager.getNavManager().getNavData(player).getActualInventory());
        return true;
    }
}
