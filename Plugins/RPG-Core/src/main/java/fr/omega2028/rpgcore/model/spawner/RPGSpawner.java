package fr.omega2028.rpgcore.model.spawner;

import fr.omega2028.rpgcore.model.monsters.RPGMonster;
import fr.omega2028.rpgcore.model.monsters.RPGMonsterBuilder;
import fr.omega2028.rpgcore.utils.EntityRangeCheck;
import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;

import java.util.Collection;

/**
 * Spawner for RPG monster, for automatic version see AutomaticRPGSpawner
 * @see RPGAutomaticSpawner
 */
public class RPGSpawner {
    protected final String spawnerID;
    protected String spawnerName;
    protected Location spawnLoc;
    protected RPGMonster rpgMonster;

    protected int checkIfCanSpawnReach;
    protected int maxMobsInZone;

    public RPGSpawner(String spawnerID) {
        this.spawnerID = spawnerID;
    }

    /**
     * Spawn a RPG monster exactly at the location of the spawner
     */
    public void spawnRPGMonster(){
        Collection<LivingEntity> found =
                EntityRangeCheck.findSpecificNearbyRPGMonster(spawnLoc, checkIfCanSpawnReach, rpgMonster);
        if (found.size() >= maxMobsInZone) return;
        RPGMonsterBuilder.buildRPGMonster(spawnLoc, rpgMonster, true);
    }

    public String getSpawnerID() {
        return spawnerID;
    }

    public String getSpawnerName() {
        return spawnerName;
    }

    public void setSpawnerName(String spawnerName) {
        this.spawnerName = spawnerName;
    }

    public Location getSpawnLoc() {
        return spawnLoc;
    }

    public void setSpawnLoc(Location spawnLoc) {
        this.spawnLoc = spawnLoc;
    }

    public RPGMonster getRpgMonster() {
        return rpgMonster;
    }

    public void setRpgMonster(RPGMonster rpgMonster) {
        this.rpgMonster = rpgMonster;
    }

    public int getCheckIfCanSpawnReach() {
        return checkIfCanSpawnReach;
    }

    public void setCheckIfCanSpawnReach(int checkIfCanSpawnReach) {
        this.checkIfCanSpawnReach = checkIfCanSpawnReach;
    }

    public int getMaxMobsInZone() {
        return maxMobsInZone;
    }

    public void setMaxMobsInZone(int maxMobsInZone) {
        this.maxMobsInZone = maxMobsInZone;
    }
}
