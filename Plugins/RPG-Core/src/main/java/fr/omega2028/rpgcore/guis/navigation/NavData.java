package fr.omega2028.rpgcore.guis.navigation;

import fr.minuskube.inv.SmartInventory;
import fr.omega2028.rpgcore.model.dungeon.Drop;
import fr.omega2028.rpgcore.model.dungeon.RPGDjRoom;
import fr.omega2028.rpgcore.model.dungeon.RPGDungeon;
import fr.omega2028.rpgcore.model.items.RPGItem;

public class NavData {
    private SmartInventory actualInventory;
    private RPGDungeon actualDj;
    private Drop actualDrop;
    private RPGDjRoom actualRoom;
    private RPGItem actualItem;

    public SmartInventory getActualInventory() {
        return actualInventory;
    }

    public void setActualInventory(SmartInventory actualInventory) {
        this.actualInventory = actualInventory;
    }

    public RPGDungeon getActualDj() {
        return actualDj;
    }

    public void setActualDj(RPGDungeon actualDj) {
        this.actualDj = actualDj;
    }

    public Drop getActualDrop() {
        return actualDrop;
    }

    public void setActualDrop(Drop actualDrop) {
        this.actualDrop = actualDrop;
    }

    public RPGDjRoom getActualRoom() {
        return actualRoom;
    }

    public void setActualRoom(RPGDjRoom actualRoom) {
        this.actualRoom = actualRoom;
    }

    public RPGItem getActualItem() {
        return actualItem;
    }

    public void setActualItem(RPGItem actualItem) {
        this.actualItem = actualItem;
    }
}
