package fr.omega2028.rpgcore.utils;

import fr.omega2028.rpgcore.model.NameSpaceKeyManager;
import fr.omega2028.rpgcore.model.monsters.RPGMonster;
import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.persistence.PersistentDataType;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;

public class EntityRangeCheck {
    public static Collection<LivingEntity> findSpecificNearbyRPGMonster(Location loc, int reach,
                                                                        RPGMonster rpgMonster){
        @NotNull Collection<LivingEntity> found = loc.getNearbyLivingEntities(reach, mob -> {
            if (mob.getPersistentDataContainer().get(NameSpaceKeyManager.getRPGMonsterKey(), PersistentDataType.STRING) == null)
                return false;
            return mob.getPersistentDataContainer().get(NameSpaceKeyManager.getRPGMonsterKey(), PersistentDataType.STRING)
                    .equals(NameSpaceKeyManager.getRPGMonsterKey().getKey());
        });
        return found;
    }
}
