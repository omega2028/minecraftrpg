package fr.omega2028.rpgcore.game;

import fr.omega2028.rpgcore.model.RPGCatalogManager;
import fr.omega2028.rpgcore.model.items.RPGItem;
import fr.omega2028.rpgcore.model.items.Rarety;
import fr.omega2028.rpgcore.model.items.comportement.concrete.LineWithEffectComportement;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffectType;

import java.util.List;

public class StaticItemGenerator {
    public static void generateStaticItems(RPGCatalogManager rpgCatalogManager){
        generateLevitationWand(rpgCatalogManager);
    }

    private static void generateLevitationWand(RPGCatalogManager rpgCatalogManager) {
        RPGItem rpgItem;
        String id = "levitation-wand";
        ItemStack levitationWand = new ItemStack(Material.STICK);
        ItemMeta levitationWandMeta = levitationWand.getItemMeta();
        levitationWandMeta.setDisplayName(ChatColor.WHITE + "" + ChatColor.BOLD + "[ Bâton de levitation ]");
        levitationWandMeta.setLore(List.of(
                ChatColor.GRAY + "Fais voler tout ce qui se trouve dans",
                ChatColor.GRAY + "sa zone d'effet, gare à l'atterrisage !"));
        levitationWandMeta.addEnchant(Enchantment.ARROW_KNOCKBACK, 20, true);
        levitationWandMeta.setUnbreakable(true);
        levitationWandMeta.setCustomModelData(1);
        levitationWand.setItemMeta(levitationWandMeta);
        rpgItem = new RPGItem(id);
        rpgItem.setBaseItem(levitationWand);
        rpgItem.setRarety(Rarety.LEGENDARY);
        rpgItem.setLvlRequired(56);
        rpgItem.addComportement(new LineWithEffectComportement(10, 1, Particle.SPELL,
                1, PotionEffectType.LEVITATION, 5, 5));
        rpgCatalogManager.getItemCatalog().put(id, rpgItem);
    }
}
