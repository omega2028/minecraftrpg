package fr.omega2028.rpgcore.utils;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;

public class ItemUtils {
    public static ItemStack makeItem(Material material, String name) {
        ItemStack it = new ItemStack(material);
        ItemMeta itM = it.getItemMeta();
        itM.setDisplayName(name);
        it.setItemMeta(itM);
        return it;
    }

    public static ItemStack makeGlowItemWithDescription(Material material, String name, List<String> lore){
        ItemStack it = new ItemStack(material);
        ItemMeta itM = it.getItemMeta();
        itM.setDisplayName(name);
        itM.setLore(lore);
        itM.addEnchant(Enchantment.ARROW_FIRE, 1, false);
        itM.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        it.setItemMeta(itM);
        return it;
    }

    public static ItemStack makeItemWithEnchantAndDescription(Material material, String name,
                                                              Enchantment enchantment, int lvl, List<String> lore) {
        ItemStack it = new ItemStack(material);
        ItemMeta itM = it.getItemMeta();
        itM.setDisplayName(name);
        itM.addEnchant(enchantment, lvl, true);
        itM.setLore(lore);
        it.setItemMeta(itM);
        return it;
    }

    public static ItemStack makeItemAndDescription(Material material, String name, List<String> lore) {
        ItemStack it = new ItemStack(material);
        ItemMeta itM = it.getItemMeta();
        itM.setDisplayName(name);
        itM.setLore(lore);
        it.setItemMeta(itM);
        return it;
    }

    public static String IntegerToRoman(int input) {
        if (input < 1 || input > 3999)
            return "Max Level";
        String s = "";
        while (input >= 1000) {
            s += "M";
            input -= 1000;
        }
        while (input >= 900) {
            s += "CM";
            input -= 900;
        }
        while (input >= 500) {
            s += "D";
            input -= 500;
        }
        while (input >= 400) {
            s += "CD";
            input -= 400;
        }
        while (input >= 100) {
            s += "C";
            input -= 100;
        }
        while (input >= 90) {
            s += "XC";
            input -= 90;
        }
        while (input >= 50) {
            s += "L";
            input -= 50;
        }
        while (input >= 40) {
            s += "XL";
            input -= 40;
        }
        while (input >= 10) {
            s += "X";
            input -= 10;
        }
        while (input >= 9) {
            s += "IX";
            input -= 9;
        }
        while (input >= 5) {
            s += "V";
            input -= 5;
        }
        while (input >= 4) {
            s += "IV";
            input -= 4;
        }
        while (input >= 1) {
            s += "I";
            input -= 1;
        }
        return s;
    }
}
