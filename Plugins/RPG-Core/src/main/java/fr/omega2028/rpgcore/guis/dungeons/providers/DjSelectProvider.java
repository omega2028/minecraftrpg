package fr.omega2028.rpgcore.guis.dungeons.providers;

import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.content.InventoryContents;
import fr.minuskube.inv.content.InventoryProvider;
import fr.minuskube.inv.content.Pagination;
import fr.minuskube.inv.content.SlotIterator;
import fr.omega2028.rpgcore.guis.navigation.GUIManager;
import fr.omega2028.rpgcore.model.RPGCatalogManager;
import fr.omega2028.rpgcore.model.dungeon.RPGDungeon;
import fr.omega2028.rpgcore.utils.ItemUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Arrays;
import java.util.UUID;

public class DjSelectProvider implements InventoryProvider {
    private final RPGCatalogManager rpgCatalogManager;
    private final GUIManager guiManager;
    private final JavaPlugin plugin;

    public DjSelectProvider(RPGCatalogManager rpgCatalogManager, GUIManager guiManager, JavaPlugin plugin) {
        this.rpgCatalogManager = rpgCatalogManager;
        this.guiManager = guiManager;
        this.plugin = plugin;
    }

    @Override
    public void init(Player player, InventoryContents inventoryContents) {
        Pagination pagination = inventoryContents.pagination();
        buildDecoration(inventoryContents);
        createQuitButton(player, inventoryContents);
        createAddButton(player, inventoryContents);
        fillDjPagination(pagination, player, inventoryContents);
        createNavButtons(inventoryContents, pagination, player);
    }

    @Override
    public void update(Player player, InventoryContents inventoryContents) {

    }

    private void createNavButtons(InventoryContents contents, Pagination pagination, Player player) {
        contents.set(5, 3, ClickableItem.of(ItemUtils.makeItem(Material.ARROW,
                ChatColor.WHITE + "Previous"),
                e -> guiManager.getDjSelectionGUI().open(player, pagination.previous().getPage())));
        contents.set(5, 5, ClickableItem.of(ItemUtils.makeItem(Material.ARROW,
                ChatColor.WHITE + "Next"),
                e -> guiManager.getDjSelectionGUI().open(player, pagination.next().getPage())));
    }

    private void fillDjPagination(Pagination pagination, Player player, InventoryContents inventoryContents) {
        ClickableItem[] items = new ClickableItem[rpgCatalogManager.getDungeonCatalog().values().size()];
        int index = 0;
        for (RPGDungeon rpgDungeon : rpgCatalogManager.getDungeonCatalog().values()){
            items[index] = ClickableItem.of(ItemUtils.makeItem(rpgDungeon.getIcon().getType(),
                    rpgDungeon.getName()), action -> {
                guiManager.getNavManager().getNavData(player).setActualDj(rpgDungeon);
                guiManager.getNavManager().openInventory(player, guiManager.getDjEditGUI());
            });
            index++;
        }
        pagination.setItems(items);
        pagination.setItemsPerPage(27);
        pagination.addToIterator(inventoryContents.newIterator(SlotIterator.Type.HORIZONTAL, 1, 0));
    }

    private void createAddButton(Player player, InventoryContents inventoryContents) {
        inventoryContents.set(5, 8, ClickableItem.of(ItemUtils.makeItem(Material.LECTERN,
                ChatColor.WHITE + "Create new dungeon"), action -> {
            UUID uuid = UUID.randomUUID();
            rpgCatalogManager.getDungeonCatalog().put(uuid.toString(), new RPGDungeon(uuid.toString()));
            guiManager.getNavManager().refreshGUI(player);
        }));
    }

    private void buildDecoration(InventoryContents inventoryContents){
        ItemStack deco = ItemUtils.makeItem(Material.BLACK_STAINED_GLASS_PANE, " ");
        for (int slot : Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 36, 37, 43, 44, 46, 47, 49, 51, 52)){
            inventoryContents.set(slot / 9, slot % 9, ClickableItem.empty(deco));
        }
    }

    private void createQuitButton(Player player, InventoryContents inventoryContents) {
        inventoryContents.set(5, 0, ClickableItem.of(ItemUtils.makeItem(Material.RED_WOOL,
                ChatColor.RED + "QUIT"), action -> {
            guiManager.getNavManager().closeInventory(player);
        }));
    }
}
