package fr.omega2028.rpgcore.guis.navigation;

import fr.minuskube.inv.SmartInventory;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.hover.content.Text;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class NavManager {
    private final Map<UUID, NavData> playerNavigationData = new HashMap<>();

    public boolean hasNavData(Player player) {
        return playerNavigationData.containsKey(player.getUniqueId());
    }

    public void createNavData(Player player){
        if (playerNavigationData.containsKey(player.getUniqueId()))
            playerNavigationData.get(player.getUniqueId()).setActualInventory(null);
        else
            playerNavigationData.put(player.getUniqueId(), new NavData());
    }

    public NavData getNavData(Player player){
        return playerNavigationData.get(player.getUniqueId());
    }

    public void closeInventory(Player player){
        if (!playerNavigationData.containsKey(player.getUniqueId())) return;
        playerNavigationData.get(player.getUniqueId()).getActualInventory().close(player);
    }

    public void openInventory(Player player, SmartInventory smartInventory){
        if (!playerNavigationData.containsKey(player.getUniqueId()))
            createNavData(player);
        playerNavigationData.get(player.getUniqueId()).setActualInventory(smartInventory);
        smartInventory.open(player);
    }

    public void refreshGUI(Player player){
        if (!playerNavigationData.containsKey(player.getUniqueId())) return;
        openInventory(player, playerNavigationData.get(player.getUniqueId()).getActualInventory());
    }

    public void messageReopenGUI(Player player){
        TextComponent msg = new TextComponent(ChatColor.GOLD + "" + ChatColor.BOLD + "[Re-ouvrir a la page]");
        msg.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text(ChatColor.GOLD + "Cliques")));
        msg.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/rpg reopengui"));
        player.sendMessage(msg);
    }
}
