package fr.omega2028.rpgcore.guis.dungeons.providers.dungeon;

import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.content.InventoryContents;
import fr.minuskube.inv.content.InventoryProvider;
import fr.omega2028.rpgcore.guis.navigation.GUIManager;
import fr.omega2028.rpgcore.model.RPGCatalogManager;
import fr.omega2028.rpgcore.utils.ItemUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.List;

public class DjDelConfirmProvider implements InventoryProvider {
    private final RPGCatalogManager rpgCatalogManager;
    private final GUIManager guiManager;

    public DjDelConfirmProvider(RPGCatalogManager rpgCatalogManager, GUIManager guiManager) {
        this.rpgCatalogManager = rpgCatalogManager;
        this.guiManager = guiManager;
    }

    @Override
    public void init(Player player, InventoryContents contents) {
        buildDecoration(contents);
        buildDeleteButton(player, contents);
        buildCancelButton(player, contents);
    }

    private void buildCancelButton(Player player, InventoryContents contents) {
        contents.set(1, 2, ClickableItem.of(ItemUtils.makeItemAndDescription(Material.RED_WOOL,
                ChatColor.RED + "Annuler la supression",
                List.of(ChatColor.RED + "retour au menu d'édition du donjon")), action -> {
            guiManager.getNavManager().openInventory(player, guiManager.getDjEditGUI());
        }));
    }

    private void buildDeleteButton(Player player, InventoryContents contents) {
        contents.set(1, 6, ClickableItem.of(ItemUtils.makeItemAndDescription(Material.GREEN_WOOL,
                ChatColor.RED + "Supprimer", List.of(ChatColor.RED + "supprime "
                        + ChatColor.BOLD + "DEFINITIVEMENT" + ChatColor.RED + " le donjon")), action -> {
                rpgCatalogManager.getDungeonCatalog().remove(
                        guiManager.getNavManager().getNavData(player).getActualDj().getDjID());
                guiManager.getNavManager().openInventory(player, guiManager.getDjSelectionGUI());
        }));
    }

    @Override
    public void update(Player player, InventoryContents contents) {

    }

    private void buildDecoration(InventoryContents inventoryContents){
        ItemStack deco = ItemUtils.makeItem(Material.BLACK_STAINED_GLASS_PANE, " ");
        for (int slot : Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26)){
            inventoryContents.set(slot / 9, slot % 9, ClickableItem.empty(deco));
        }
    }
}
