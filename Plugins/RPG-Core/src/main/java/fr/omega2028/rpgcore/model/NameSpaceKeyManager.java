package fr.omega2028.rpgcore.model;

import org.bukkit.NamespacedKey;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Manage all namespace keys in this plugin
 * Instantiate It at the start of the plugin
 */
public class NameSpaceKeyManager {
    private static NamespacedKey RPGItemKey;
    private static NamespacedKey RPGMonsterKey;

    public static void createNSPKeys(JavaPlugin plugin){
        RPGItemKey = new NamespacedKey(plugin, "rpg_item_id");
        RPGMonsterKey = new NamespacedKey(plugin, "rpg_monster_id");
    }

    public static NamespacedKey getRPGItemKey() {
        return RPGItemKey;
    }

    public static NamespacedKey getRPGMonsterKey() {
        return RPGMonsterKey;
    }
}
