package fr.omega2028.rpgcore.tasks;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Map;

public class PointIndicator {
    protected final JavaPlugin plugin;
    protected final Map<Player, Integer> playerShow = new HashMap<>();

    public PointIndicator(JavaPlugin plugin){
        this.plugin = plugin;
    }

    public void showPoint(Player player, Location pointLoc){
        int task = Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, () -> {
            player.spawnParticle(Particle.VILLAGER_ANGRY, pointLoc, 1);
        }, 0L, 2L);
        playerShow.put(player, task);
    }

    public void hidePoints(Player player){
        while (playerShow.containsKey(player)) {
            Bukkit.getScheduler().cancelTask(playerShow.get(player));
            playerShow.remove(player);
        }
    }
}
