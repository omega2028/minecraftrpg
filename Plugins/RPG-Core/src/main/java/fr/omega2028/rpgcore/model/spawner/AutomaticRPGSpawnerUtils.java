package fr.omega2028.rpgcore.model.spawner;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Manage Automatic RPG Spawners
 */
public class AutomaticRPGSpawnerUtils {
    private final Map<RPGAutomaticSpawner, Integer> autoRPGSpawners = new HashMap<>();
    private final JavaPlugin plugin;

    /**
     * Constructor of AutomaticRPGSpawnerManager
     * @param plugin
     */
    public AutomaticRPGSpawnerUtils(JavaPlugin plugin){
        this.plugin = plugin;
    }

    /**
     * Activate the AutomaticRPGSpawner
     * @param autoRPGSpawner
     */
    public void activateAutoRPGSpawner(RPGAutomaticSpawner autoRPGSpawner){
        if (autoRPGSpawners.containsKey(autoRPGSpawner)) return;
        int task = Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, () -> {
            if (autoRPGSpawner.isSpawnerActivated()){
                autoRPGSpawner.spawnRPGMonster();
            }
        }, 0L, autoRPGSpawner.getSecondsBetweenSpawn()*20L);
        autoRPGSpawners.put(autoRPGSpawner, task);
    }

    /**
     * stop the task of a AutomaticRPGSpawner
     * @param autoRPGSpawner
     */
    private void cancelAutoRPGSpawnerTask(RPGAutomaticSpawner autoRPGSpawner){
        Bukkit.getScheduler().cancelTask(autoRPGSpawners.get(autoRPGSpawner));
    }

    /**
     * destroy a AutomaticRPGSpawner
     * @param spawnerName
     */
    public void destroyAutoRPGSpawner(String spawnerName){
        for (RPGAutomaticSpawner autoRPGSpawner : autoRPGSpawners.keySet()){
            if (autoRPGSpawner.getSpawnerName().equals(spawnerName)) {
                cancelAutoRPGSpawnerTask(autoRPGSpawner);
                autoRPGSpawners.remove(autoRPGSpawner);
                return;
            }
        }
    }

    public List<String> getAutoRPGSpawnerIDs() {
        List<String> autoRPGSpawnerNames = new ArrayList<>();
        for (RPGAutomaticSpawner autoRPGSpawner : autoRPGSpawners.keySet()){
            autoRPGSpawnerNames.add(autoRPGSpawner.getSpawnerName());
        }
        return autoRPGSpawnerNames;
    }
}
