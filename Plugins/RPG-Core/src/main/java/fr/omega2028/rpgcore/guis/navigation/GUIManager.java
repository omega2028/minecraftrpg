package fr.omega2028.rpgcore.guis.navigation;

import fr.minuskube.inv.InventoryManager;
import fr.minuskube.inv.SmartInventory;
import fr.omega2028.rpgcore.guis.dungeons.providers.DjSelectProvider;
import fr.omega2028.rpgcore.guis.dungeons.providers.drops.DjChooseItemToDropProvider;
import fr.omega2028.rpgcore.guis.dungeons.providers.drops.DjEditDropChanceProvider;
import fr.omega2028.rpgcore.guis.dungeons.providers.drops.DjEditDropGUIProvider;
import fr.omega2028.rpgcore.guis.dungeons.providers.drops.DjSelectDropsProvider;
import fr.omega2028.rpgcore.guis.dungeons.providers.dungeon.DjDelConfirmProvider;
import fr.omega2028.rpgcore.guis.dungeons.providers.dungeon.DjEditProvider;
import fr.omega2028.rpgcore.guis.dungeons.providers.dungeon.DjXpProvider;
import fr.omega2028.rpgcore.guis.dungeons.providers.rooms.DjEditRoomProvider;
import fr.omega2028.rpgcore.guis.dungeons.providers.rooms.DjSelectRoomsProvider;
import fr.omega2028.rpgcore.guis.items.providers.EditItemGUIProvider;
import fr.omega2028.rpgcore.guis.items.providers.EditItemLvlRequiredGUIProvider;
import fr.omega2028.rpgcore.guis.items.providers.GiveItemSelectedGUIProvider;
import fr.omega2028.rpgcore.model.RPGCatalogManager;
import org.bukkit.ChatColor;
import org.bukkit.conversations.ConversationFactory;
import org.bukkit.plugin.java.JavaPlugin;

public class GUIManager {
    private final RPGCatalogManager rpgCatalogManager;
    private final InventoryManager inventoryManager;
    private final ConversationFactory conversationFactory;
    private final JavaPlugin plugin;
    private final NavManager navManager = new NavManager();

    /**
     * Dungeons GUIs
     */
    private final SmartInventory djSelectionGUI;
    private final SmartInventory djEditGUI;
    private final SmartInventory djDelConfirmGUI;
    private final SmartInventory djXpGUI;
    /**
     * Dungeon Rooms GUIs
     */
    private final SmartInventory djSelectRoomsGUI;
    private final SmartInventory djEditRoomProvider;
    /**
     * Dungeon Drops GUIs
     */
    private final SmartInventory djChooseItemToDrop;
    private final SmartInventory djSelectDropsGUI;
    private final SmartInventory djEditDropGUI;
    private final SmartInventory djEditDropChanceGUI;

    /**
     * Items GUIs
     */
    private final SmartInventory giveItemSelectedGUI;
    private final SmartInventory editItemGUI;
    private final SmartInventory editItemLvlRequiredGUI;

    public GUIManager(RPGCatalogManager rpgCatalogManager, InventoryManager inventoryManager,
                      ConversationFactory conversationFactory, JavaPlugin plugin){
        this.inventoryManager = inventoryManager;
        inventoryManager.init();
        this.rpgCatalogManager = rpgCatalogManager;
        this.conversationFactory = conversationFactory;
        this.plugin = plugin;

        //dungeons
        djSelectionGUI = buildDjSelectionGUI();
        djEditGUI = buildDjEditGUI();
        djDelConfirmGUI = buildDjDelConfirmGUI();
        djXpGUI = buildDjXpGUI();
        //dungeons rooms
        djSelectRoomsGUI = buildDjSelectRoomsGUI();
        djEditRoomProvider = buildDjEditRoomGUI();
        //dungeons drops
        djSelectDropsGUI = buildDjSelectDropsGUI();
        djEditDropGUI = buildDjEditDropGUI();
        djChooseItemToDrop = buildDjChooseItemToDropGUI();
        djEditDropChanceGUI = buildDjEditDropChanceGUI();

        //items
        giveItemSelectedGUI = buildGiveItemSelectedGUI();
        editItemGUI = buildEditItemGUI();
        editItemLvlRequiredGUI = buildEditItemLvlRequiredGUI();
    }

    private SmartInventory buildEditItemLvlRequiredGUI() {
        return SmartInventory.builder().manager(inventoryManager)
                .provider(new EditItemLvlRequiredGUIProvider(this))
                .title(ChatColor.BOLD + "Chance de drop l'item")
                .size(4, 9)
                .closeable(false)
                .build();
    }

    private SmartInventory buildDjEditDropChanceGUI() {
        return SmartInventory.builder().manager(inventoryManager)
                .provider(new DjEditDropChanceProvider(this))
                .title(ChatColor.BOLD + "Chance de drop l'item")
                .size(4, 9)
                .closeable(false)
                .build();
    }

    private SmartInventory buildDjXpGUI() {
        return SmartInventory.builder().manager(inventoryManager)
                .provider(new DjXpProvider(this))
                .title(ChatColor.BOLD + "XP reçue en fin de donjon")
                .size(4, 9)
                .closeable(false)
                .build();
    }

    private SmartInventory buildEditItemGUI() {
        return SmartInventory.builder().manager(inventoryManager)
                .provider(new EditItemGUIProvider(this, rpgCatalogManager))
                .title(ChatColor.BOLD + "Edition d'un item")
                .size(3, 9)
                .closeable(false)
                .build();
    }

    private SmartInventory buildDjEditRoomGUI() {
        return SmartInventory.builder().manager(inventoryManager)
                .provider(new DjEditRoomProvider(this, plugin))
                .title(ChatColor.BOLD + "Edition d'une room")
                .size(6, 9)
                .closeable(false)
                .build();
    }

    private SmartInventory buildDjChooseItemToDropGUI() {
        return SmartInventory.builder().manager(inventoryManager)
                .provider(new DjChooseItemToDropProvider(this, rpgCatalogManager))
                .title(ChatColor.BOLD + "Sélectionne l'item à drop")
                .size(6, 9)
                .closeable(false)
                .build();
    }

    private SmartInventory buildGiveItemSelectedGUI() {
        return SmartInventory.builder().manager(inventoryManager)
                .provider(new GiveItemSelectedGUIProvider(this, rpgCatalogManager))
                .title(ChatColor.BOLD + "Sélectionne l'item à give")
                .size(6, 9)
                .closeable(false)
                .build();
    }

    private SmartInventory buildDjEditDropGUI() {
        return SmartInventory.builder().manager(inventoryManager)
                .provider(new DjEditDropGUIProvider(this))
                .title(ChatColor.BOLD + "Edition d'un drop")
                .size(3, 9)
                .closeable(false)
                .build();
    }

    private SmartInventory buildDjSelectDropsGUI() {
        return SmartInventory.builder().manager(inventoryManager)
                .provider(new DjSelectDropsProvider(this))
                .title(ChatColor.BOLD + "Sélection drops du donjon")
                .size(6, 9)
                .closeable(false)
                .build();
    }

    private SmartInventory buildDjSelectRoomsGUI() {
        return SmartInventory.builder().manager(inventoryManager)
                .provider(new DjSelectRoomsProvider(this))
                .title(ChatColor.BOLD + "Sélectionne une room")
                .size(6, 9)
                .closeable(false)
                .build();
    }

    private SmartInventory buildDjDelConfirmGUI() {
        return SmartInventory.builder().manager(inventoryManager)
                .provider(new DjDelConfirmProvider(rpgCatalogManager, this))
                .title(ChatColor.RED + "" + ChatColor.BOLD + "Supprimer ce donjon ?")
                .size(3, 9)
                .closeable(false)
                .build();
    }

    private SmartInventory buildDjEditGUI() {
        return SmartInventory.builder().manager(inventoryManager)
                .provider(new DjEditProvider(rpgCatalogManager, this, plugin))
                .title(ChatColor.BOLD + "Edition de donjon")
                .size(6, 9)
                .closeable(false)
                .build();
    }

    private SmartInventory buildDjSelectionGUI() {
        return SmartInventory.builder().manager(inventoryManager)
                .provider(new DjSelectProvider(rpgCatalogManager, this, plugin))
                .title(ChatColor.BOLD + "Sélectionne un donjon")
                .size(6, 9)
                .closeable(false)
                .build();
    }

    public SmartInventory getDjSelectionGUI() {
        return djSelectionGUI;
    }

    public ConversationFactory getConversationFactory() {
        return conversationFactory;
    }

    public NavManager getNavManager() {
        return navManager;
    }

    public SmartInventory getDjEditGUI() {
        return djEditGUI;
    }

    public SmartInventory getDjDelConfirmGUI() {
        return djDelConfirmGUI;
    }

    public SmartInventory getDjSelectRoomsGUI() {
        return djSelectRoomsGUI;
    }

    public SmartInventory getDjSelectDropsGUI() {
        return djSelectDropsGUI;
    }

    public SmartInventory getDjEditDropGUI() {
        return djEditDropGUI;
    }

    public SmartInventory getGiveItemSelectedGUI() {
        return giveItemSelectedGUI;
    }

    public SmartInventory getDjChooseItemToDrop() {
        return djChooseItemToDrop;
    }

    public SmartInventory getDjEditRoomProvider() {
        return djEditRoomProvider;
    }

    public SmartInventory getEditItemGUI() {
        return editItemGUI;
    }

    public SmartInventory getDjXpGUI() {
        return djXpGUI;
    }

    public SmartInventory getDjEditDropChanceGUI() {
        return djEditDropChanceGUI;
    }

    public SmartInventory getEditItemLvlRequiredGUI() {
        return editItemLvlRequiredGUI;
    }
}
