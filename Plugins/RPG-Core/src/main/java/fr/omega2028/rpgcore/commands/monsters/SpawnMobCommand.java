package fr.omega2028.rpgcore.commands.monsters;

import fr.omega2028.rpgcore.model.RPGCatalogManager;
import fr.omega2028.rpgcore.model.monsters.RPGMonster;
import fr.omega2028.rpgcore.model.monsters.RPGMonsterBuilder;
import fr.omega2028.rpgcore.utils.Decorator;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

/**
 * Spawn a RPG monster at the player's location if the monster exists
 */
public class SpawnMobCommand implements CommandExecutor {
    private final RPGCatalogManager rpgCatalogManager;

    public SpawnMobCommand(RPGCatalogManager rpgCatalogManager){
        this.rpgCatalogManager = rpgCatalogManager;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command,
                             @NotNull String label, @NotNull String[] args) {
        if (!(sender instanceof Player player) || args.length < 2) return false;
        if (!rpgCatalogManager.getMonsterCatalog().containsKey(args[1])){
            player.sendMessage(Decorator.convertError("This monster doesn't exists !"));
            return true;
        }
        RPGMonster rpgMonster = rpgCatalogManager.getMonsterCatalog().get(args[1]);
        if (rpgMonster == null){
            player.sendMessage(Decorator.convertError("This monster doesn't exists !"));
            return true;
        }
        int nbSpawn = 1;
        try {
            nbSpawn = Integer.parseInt(args[2]);
        }
        catch (NumberFormatException ignored){ }

        for (int i = 0; i < nbSpawn; i++)
            RPGMonsterBuilder.buildRPGMonster(player.getLocation(), rpgMonster, false);
        if (nbSpawn == 1)
            player.sendMessage(Decorator.convert("Monstre généré"));
        else
            player.sendMessage(Decorator.convert("Monstres générés " + "(" + nbSpawn + ")"));
        return true;
    }
}