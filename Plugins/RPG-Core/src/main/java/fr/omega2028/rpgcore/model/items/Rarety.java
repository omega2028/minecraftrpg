package fr.omega2028.rpgcore.model.items;

public enum Rarety {
    COMMON,
    UNCOMMON,
    RARE,
    EPIC,
    LEGENDARY,
    ANACRONIC,
    ADMIN
}
