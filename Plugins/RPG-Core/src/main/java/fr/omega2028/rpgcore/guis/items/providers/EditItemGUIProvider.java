package fr.omega2028.rpgcore.guis.items.providers;

import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.content.InventoryContents;
import fr.minuskube.inv.content.InventoryProvider;
import fr.omega2028.rpgcore.guis.navigation.GUIManager;
import fr.omega2028.rpgcore.model.RPGCatalogManager;
import fr.omega2028.rpgcore.model.items.RPGItem;
import fr.omega2028.rpgcore.model.items.RPGItemUtils;
import fr.omega2028.rpgcore.model.items.Rarety;
import fr.omega2028.rpgcore.utils.Decorator;
import fr.omega2028.rpgcore.utils.ItemUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.List;

public class EditItemGUIProvider implements InventoryProvider {
    private final GUIManager guiManager;
    private final RPGCatalogManager rpgCatalogManager;

    public EditItemGUIProvider(GUIManager guiManager, RPGCatalogManager rpgCatalogManager) {
        this.guiManager = guiManager;
        this.rpgCatalogManager = rpgCatalogManager;
    }

    @Override
    public void init(Player player, InventoryContents contents) {
        buildDecoration(contents);
        createBackButton(player, contents);
        buildRPGIcon(player, contents);
        buildRaretyChanger(player, contents);
        buildDeleteItemBtn(player, contents);
        buildLevelSelector(player, contents);
    }

    private void buildLevelSelector(Player player, InventoryContents contents) {
        contents.set(1, 6, ClickableItem.of(ItemUtils.makeItemAndDescription(Material.EXPERIENCE_BOTTLE,
                ChatColor.GOLD + "Niveau requis",
                List.of(ChatColor.GRAY + "Changer le niveau requis",
                        ChatColor.GRAY + "Niveau requis actuel : "
                        + ChatColor.GOLD + "" + ChatColor.BOLD
                                + guiManager.getNavManager().getNavData(player).getActualItem().getLvlRequired())),
                action -> {
                    guiManager.getNavManager().openInventory(player, guiManager.getEditItemLvlRequiredGUI());
        }));
    }

    private void buildDeleteItemBtn(Player player, InventoryContents contents) {
        contents.set(1, 0, ClickableItem.of(ItemUtils.makeItemAndDescription(Material.BARRIER,
                ChatColor.RED + "DELETE", List.of(ChatColor.RED + "/!\\ la suppression est ",
                        ChatColor.RED + "" + ChatColor.BOLD + "DEFINITIVE")), action -> {
            rpgCatalogManager.getItemCatalog().remove(guiManager.getNavManager().getNavData(player).getActualItem().getRpgID());
            guiManager.getNavManager().openInventory(player, guiManager.getGiveItemSelectedGUI());
        }));
    }

    private void buildRaretyChanger(Player player, InventoryContents contents) {
        RPGItem actualItem = guiManager.getNavManager().getNavData(player).getActualItem();
        contents.set(1, 4, ClickableItem.of(ItemUtils.makeItemAndDescription(Material.FIRE_CORAL,
                ChatColor.RED + "Changer la rareté", List.of(Decorator.convertRarety(actualItem.getRarety()))),
                action -> {
                    switch (actualItem.getRarety()) {
                        case COMMON -> actualItem.setRarety(Rarety.UNCOMMON);
                        case UNCOMMON -> actualItem.setRarety(Rarety.RARE);
                        case RARE -> actualItem.setRarety(Rarety.EPIC);
                        case EPIC -> actualItem.setRarety(Rarety.LEGENDARY);
                        case LEGENDARY -> actualItem.setRarety(Rarety.ANACRONIC);
                        default -> actualItem.setRarety(Rarety.COMMON);
                    }
                    guiManager.getNavManager().refreshGUI(player);
                }));
    }

    private void buildRPGIcon(Player player, InventoryContents contents) {
        contents.set(1, 2, ClickableItem.empty(RPGItemUtils.buildDisplayRPGItem(
                guiManager.getNavManager().getNavData(player).getActualItem())));
    }

    @Override
    public void update(Player player, InventoryContents contents) {

    }

    private void buildDecoration(InventoryContents inventoryContents){
        ItemStack deco = ItemUtils.makeItem(Material.BLACK_STAINED_GLASS_PANE, " ");
        for (int slot : Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 10, 16, 18, 19, 20, 21, 22, 23, 24, 25, 26)){
            inventoryContents.set(slot / 9, slot % 9, ClickableItem.empty(deco));
        }
    }

    private void createBackButton(Player player, InventoryContents inventoryContents) {
        inventoryContents.set(1, 8, ClickableItem.of(ItemUtils.makeItem(Material.GREEN_CONCRETE,
                ChatColor.GREEN + "SAVE"), action -> {
            guiManager.getNavManager().openInventory(player, guiManager.getGiveItemSelectedGUI());
        }));
    }
}
