package fr.omega2028.rpgcore.persist;

import fr.omega2028.rpgcore.model.RPGCatalogManager;
import fr.omega2028.rpgcore.model.items.RPGItem;
import fr.omega2028.rpgcore.model.items.Rarety;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;
import java.util.Map;

public class ItemLoader {
    private JavaPlugin plugin;
    private String itmPath = "rpgitems.";

    public ItemLoader(JavaPlugin javaPlugin) {
        this.plugin = javaPlugin;
    }

    public void load(RPGCatalogManager rpgCatalogManager) {
        Bukkit.getLogger().info("appel de la meth load");
        Config itmConfig = new Config(plugin, "Items/", "rpgitems");
        FileConfiguration configFile = itmConfig.getEditableConfigFile();
        List<String> ids = configFile.getStringList(itmPath + "ids");
        Bukkit.getLogger().info("ids lus : " + ids);
        RPGItem rpgItem;
        for (String id : ids){
            Bukkit.getLogger().info(id);
            rpgItem = new RPGItem(id);
            rpgItem.setBaseItem(configFile.getItemStack(itmPath + id + ".item"));
            try{
                Rarety rarety = Rarety.valueOf(configFile.getString(itmPath + id + ".rarety"));
                rpgItem.setRarety(rarety);
            } catch (IllegalArgumentException e){
                e.printStackTrace();
                break;
            }
            rpgItem.setLvlRequired(configFile.getInt(itmPath + id + ".levelRequired"));
            rpgCatalogManager.getItemCatalog().put(id, rpgItem);
        }
    }
}
