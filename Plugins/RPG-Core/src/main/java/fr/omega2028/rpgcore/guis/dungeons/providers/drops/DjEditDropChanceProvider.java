package fr.omega2028.rpgcore.guis.dungeons.providers.drops;

import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.content.InventoryContents;
import fr.minuskube.inv.content.InventoryProvider;
import fr.omega2028.rpgcore.guis.navigation.GUIManager;
import fr.omega2028.rpgcore.utils.ItemUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;

public class DjEditDropChanceProvider implements InventoryProvider {
    private static final DecimalFormat df = new DecimalFormat("0.00");
    private final GUIManager guiManager;

    public DjEditDropChanceProvider(GUIManager guiManager) {
        this.guiManager = guiManager;
    }

    @Override
    public void init(Player player, InventoryContents contents) {
        buildDecoration(contents);
        createDropPercentDisplay(player, contents);
        createSaveBtns(player, contents);
        createDropPercentModifiers(player, contents);
    }

    @Override
    public void update(Player player, InventoryContents contents) {

    }

    private void createDropPercentDisplay(Player player, InventoryContents contents) {
        float dropProba = guiManager.getNavManager().getNavData(player).getActualDrop().getDropProba()*100;
        contents.set(2, 4, ClickableItem.empty(ItemUtils.makeItemAndDescription(Material.COMPARATOR,
                ChatColor.WHITE + "Chance de drop",
                List.of(ChatColor.GOLD + "" + ChatColor.BOLD + df.format(dropProba) + "%"))));
    }

    private void createSaveBtns(Player player, InventoryContents contents) {
        contents.set(3, 8, ClickableItem.of(ItemUtils.makeItem(Material.GREEN_CONCRETE,
                ChatColor.GREEN + "SAVE"), action -> {
            guiManager.getNavManager().openInventory(player, guiManager.getDjEditDropGUI());
        }));
        contents.set(3, 0, ClickableItem.of(ItemUtils.makeItem(Material.GREEN_CONCRETE,
                ChatColor.GREEN + "SAVE"), action -> {
            guiManager.getNavManager().openInventory(player, guiManager.getDjEditDropGUI());
        }));
    }

    private void buildDecoration(InventoryContents inventoryContents){
        ItemStack deco = ItemUtils.makeItem(Material.BLACK_STAINED_GLASS_PANE, " ");
        for (int slot : Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 17, 18, 19, 25, 26, 28, 29, 30, 31, 32, 33, 34)){
            inventoryContents.set(slot / 9, slot % 9, ClickableItem.empty(deco));
        }
    }

    private void createDropPercentModifiers(Player player, InventoryContents contents) {
        contents.set(1, 1, ClickableItem.of(ItemUtils.makeItem(Material.BLACK_DYE,
                ChatColor.RED + "" + ChatColor.BOLD + "-10"
                        + ChatColor.BOLD + "" + ChatColor.GOLD + "%"), action -> {
            guiManager.getNavManager().getNavData(player).getActualDrop().setDropProba(
                    guiManager.getNavManager().getNavData(player).getActualDrop().getDropProba()
                            - 0.1f
            );
            guiManager.getNavManager().refreshGUI(player);
        }));
        contents.set(1, 2, ClickableItem.of(ItemUtils.makeItem(Material.INK_SAC,
                ChatColor.RED + "" + ChatColor.BOLD + "-1"
                        + ChatColor.BOLD + "" + ChatColor.GOLD + "%"), action -> {
            guiManager.getNavManager().getNavData(player).getActualDrop().setDropProba(
                    guiManager.getNavManager().getNavData(player).getActualDrop().getDropProba()
                            - 0.01f
            );
            guiManager.getNavManager().refreshGUI(player);
        }));
        contents.set(1, 3, ClickableItem.of(ItemUtils.makeItem(Material.GRAY_DYE,
                ChatColor.RED + "" + ChatColor.BOLD + "-0.1"
                        + ChatColor.BOLD + "" + ChatColor.GOLD + "%"), action -> {
            guiManager.getNavManager().getNavData(player).getActualDrop().setDropProba(
                    guiManager.getNavManager().getNavData(player).getActualDrop().getDropProba()
                            - 0.001f
            );
            guiManager.getNavManager().refreshGUI(player);
        }));
        contents.set(1, 7, ClickableItem.of(ItemUtils.makeItem(Material.BLUE_DYE,
                ChatColor.RED + "" + ChatColor.BOLD + "+10"
                        + ChatColor.BOLD + "" + ChatColor.GOLD + "%"), action -> {
            guiManager.getNavManager().getNavData(player).getActualDrop().setDropProba(
                    guiManager.getNavManager().getNavData(player).getActualDrop().getDropProba()
                            + 0.1f
            );
            guiManager.getNavManager().refreshGUI(player);
        }));
        contents.set(1, 6, ClickableItem.of(ItemUtils.makeItem(Material.GLOW_INK_SAC,
                ChatColor.RED + "" + ChatColor.BOLD + "+1"
                        + ChatColor.BOLD + "" + ChatColor.GOLD + "%"), action -> {
            guiManager.getNavManager().getNavData(player).getActualDrop().setDropProba(
                    guiManager.getNavManager().getNavData(player).getActualDrop().getDropProba()
                            + 0.01f
            );
            guiManager.getNavManager().refreshGUI(player);
        }));
        contents.set(1, 5, ClickableItem.of(ItemUtils.makeItem(Material.LIGHT_BLUE_DYE,
                ChatColor.RED + "" + ChatColor.BOLD + "+0.1"
                        + ChatColor.BOLD + "" + ChatColor.GOLD + "%"), action -> {
            guiManager.getNavManager().getNavData(player).getActualDrop().setDropProba(
                    guiManager.getNavManager().getNavData(player).getActualDrop().getDropProba()
                            + 0.001f
            );
            guiManager.getNavManager().refreshGUI(player);
        }));
    }
}
