# MinecraftRPG

## Description :
* Serveur Minecraft orienté RPG, fonctionne avec une combinaison de plugins, data packs, packs de textures. 
* Ce GIT est l'endroit ou l'on peut retrouver tout le code source des plugins "maison", mais aussi les textures, les datapacks, ...

## Equipe :
* Développeurs (H/F):
    </br>- omega2028 | omega2028#1480 [Chef de projet]
    </br>- Norzolrat | Norzolrat#0703
    </br>- Xzeno | Xzeno#4329
* Graphistes (H/F):
    </br>- Morrison Miau | Morrison Miau#8828
* Builders (H/F):
    </br>- Alexlepetii | Alexlepetii#9410
* Community Manager (H/F):
    </br>- Moon_ | Moon_#0060

## Organisation du dépot :
* Plugins : [RPG-Core](/Plugins/RPG-Core/), [RPG-Jobs](/Plugins/RPG-Jobs/)
* Textures : [TexturePack](/Textures/TexturePack)

## Pour les novices avec GIT petit tuto de base :
* Clique [ici](/GuideGIT.md) pour le consulter


