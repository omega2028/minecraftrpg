# Tutoriel pour utiliser GIT

## Pour récupérer le contenu du dépot : /!\ toujours le faire avant de faire des modifs /!\
* **git pull** : récupère ce qui se trouve sur le dépot et le fusionne avec ta version sur ton pc

## Pour poster ton travail : /!\ bien **git pull** avant et bien faire les commandes **dans l'ordre** /!\
* **git add .** : prépare toutes les modifications que tu vas envoyer
* **git commit -m "mon message pour dire ce que j'ai changé"** : prépare un commit (ta version que tu vas envoyer)
* **git push** : envoie tout ton travail sur le dépot